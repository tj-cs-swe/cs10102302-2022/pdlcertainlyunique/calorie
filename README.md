### 2022.5.11

### 部署说明：

网址：http://123.60.54.91

#### 云服务器：

![](img/cloudserver.png)

![](img/cloudserver2.png)

安全组增添配置：

![](img/safegroup.png)

#### 数据库连接：

mysql:

​		版本：8.0.29

​		mysql密码用的是pjx的密码，也就是目前dev分支里的application.yml里配置的那个密码。

​		我之前用的navicat11版本过低连接失败，后来用navicat15连接成功，教程：

​		https://www.bilibili.com/read/cv15128680

redis:

​        版本：latest

​		与之前连接redis的方式类似。

#### dev改动

​		dev后端新增application-pro.yml，用于线上部署时的项目配置，本地idea运行项目时还是默认采用application的配置。

#### POSTMAN测试例：

![](img/cloudpostman.png)

#### 参考资料

| Docker+nginx部署Springboot+vue前后端分离项目                 | https://www.zhuawaba.com/post/84                             |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| 云服务器docker部署SpringBoot+mysql+Redis项目                 | **https://blog.csdn.net/qq_42365842/article/details/124524875** |
| Docker部署Nginx                                              | **https://www.cnblogs.com/niunafei/p/12814979.html**         |
| 报错：'./docker-compose.yml', service must be a mapping, not a NoneType | **https://stackoverflow.com/questions/43452448/docker-compose-yml-service-must-be-a-mapping-not-a-nonetype** |
| navicat15                                                    | https://www.bilibili.com/read/cv15128680                     |

### **2022.5.10**

#### **后端认证相关：**

**登录才能访问的界面，前端发送请求时必须把 json web token 放在请求头部的 Authorization**
- **获取用户信息 (profile) ：**
  **![](img/user-controller-profile.png)**
  **返回值见 reponse.data.user**
  **![](img/user-controller-profile-return.png)**

- **骑手认证审核请求**
  **![](img/rider-controller-rider.png)**

- **食品商家认证审核请求**
  **![](img/food-merchant-req.png)**

- **健身商家认证审核请求**
  **![](img/gym-merchant-req.png)**

- **管理员商家认证审核**
  **![](img/admin-merchant.png)**
  **健身商家认证审核路由为 /admin/merchant/foodMerchants**
  **骑手认证审核路由为 /admin/riders**
  **三者 Dto 相同。**

- **审核状态判断**
  **根据 is... 的值来判断，如下所示：**
  ```java
  /**
     * 0: 未认证
     * 1：未认证 + 待审核
     * 2：已认证 + 待审核 （修改）
     * 3：未认证 + 审核失败
     * 4: 已认证 + 审核失败 （修改）
     * 5: 认证成功
     */
  ```

- **管理员获取审核请求:**
  **![](img/admin-get-auth-1.png)**
  **![](img/admin-get-auth-2.png)**
  **![](img/admin-get-auth-3.png)**
  **返回值为数组（名称分别为 riders, foodMerchants, gymMerchants) 如下**
  **![](img/admin-get-auth-ret.png)**

### **2022.5.8**
**后端增加账号信息修改（改密码、昵称），用户名也可以改，但会影响登录之类的**

### **2022.5.7**

#### **后端权限相关：**

**如果一个端口需要登录才能访问，则加上注解**
```java
@RequiresAuthentication
```

**如果需要权限，如 admin, rider，则加上注解**
```java
@RequiresRoles(value = {'admin', 'rider'})
```
**post man 验证时需要设置 Header 如下图所示：**
**![](img/jwt.png)**

**注意无登录或授权限制的端口，不要添加 Header.Authorization**

#### **新增邮箱验证码功能**

**新增验证码5min后失效功能**



**验证码失效功能说明：**

- **采用redis数据库来存储验证码，保存形式是*电话号码-验证码*，*邮箱-验证码* 的键值对形式。**

- **redis安装说明：**

  - **redis server下载地址：**

    **https://github.com/tporadowski/redis/releases/download/v5.0.14.1/Redis-x64-5.0.14.1.zip**

  - **redis client 下载地址：**

    **https://github.com/caoxinyu/RedisClient/blob/1.5.0/release/redisclient-win32.x86.1.5.jar**

    **注：64的各种版本在我的电脑的jdk1.7下跑不动，1.5以上的32版本则因为redis server版本过低连接失败。**

  - **redis server运行方式，在其目录下cmd执行**

    ```
    redis-server.exe redis.windows.conf
    ```

  - **redis client 连接方式：**

    - **打开jar，**
    - **右键Redis servers-Add server,** 
    - **Name随便 ，比如localhost**
    - **Host = localhost,** 
    - **Password空着**

    **最后这个效果：**

    **![](img/redisclient.png)**

- **redis程序说明：**

  - **application.yml  redis配置，不用改**

  - **com.calorie包下：**
    - **增加config/RedisConig：不用管**
    - **增加util/RedisUtil: redis增删改查**

- **用postman测试：**

  **发送验证码：**

  **![](img/redisVerifyCode.png)**

  **默认在redis的db0生成键值对，**

  **![](img/redisDB0.png)**

  ​		**5分钟后自动清除，如果5分钟内重复提交，验证码会覆盖之前的，要看到新的验证码值好像需要先把红圈的框关掉再打开才能刷新。**

  ​		**还有每次redis内容更新的时候记得在redisclient里右键refresh一下。**

  **注册测试：**

  **![](img/redisRegister.png)**
  
  **5分钟内完成注册，redis中验证码也会被清除**

------

### **past**

**项目文件在server/calorie中，数据库sql脚本在server/calorie.sql中(需要预先建立calorie数据库)**

**目前实现了基本的登录以及可以发送手机验证码的注册功能**

**目前需要连接本地数据库，版本为mysql8，相关配置在application.yml中，只需修改用户名和密码**

**数据库名称是calorie**

**postman接口测试：**       

**登录：  改成了8081接口**

**![login](img/login.png)**

**注册：**

**下面的userPhone可换成自己的电话号码，status=”1“表示前端点击了发送验证码的按钮**

**![register](img/send.png)**

**输入验证码，status="2"表示前端点击了”注册“按钮**

**检查验证码为正确后，验证码立即失效（从数据表code中删除暂存的验证码）**

**![](img/verify.png)**
