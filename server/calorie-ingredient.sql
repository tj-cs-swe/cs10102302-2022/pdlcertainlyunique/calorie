/*
 Navicat Premium Data Transfer

 Source Server         : root
 Source Server Type    : MySQL
 Source Server Version : 80026
 Source Host           : localhost:3306
 Source Schema         : calorie

 Target Server Type    : MySQL
 Target Server Version : 80026
 File Encoding         : 65001

 Date: 28/05/2022 20:47:54
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for food
-- ----------------------------
DROP TABLE IF EXISTS `food`;
CREATE TABLE `food`  (
  `food_id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `food_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `food_introduction` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `food_image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `food_category` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `food_price` float NULL DEFAULT NULL,
  `food_number` float NULL DEFAULT NULL,
  `food_calorie` float NULL DEFAULT NULL,
  `food_take_status` int NULL DEFAULT NULL,
  `food_shop_id` int NULL DEFAULT NULL,
  PRIMARY KEY (`food_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 29 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of food
-- ----------------------------
INSERT INTO `food` VALUES (1, 'ice123', 'abc', 'picture1.png', '1', 1234, 3144, 4567, 1, 1);
INSERT INTO `food` VALUES (11, 'ice', 'abc', 'picture1.png', '1', 1234, 3144, 4567, 1, 2);
INSERT INTO `food` VALUES (12, 'ice', 'abc', 'picture1.png', '1', 1234, 3144, 4567, 1, 2);
INSERT INTO `food` VALUES (13, 'ice', 'abc', 'picture1.png', '1', 1234, 3144, 4567, 1, 2);
INSERT INTO `food` VALUES (14, 'ice', 'abc', 'picture1.png', '1', 1234, 3144, 4567, 1, 2);
INSERT INTO `food` VALUES (15, 'ice', 'abc', 'picture1.png', '1', 1234, 3144, 4567, 1, 2);
INSERT INTO `food` VALUES (16, 'ice', 'abc', 'picture1.png', '1', 1234, 3144, 4567, 1, 2);
INSERT INTO `food` VALUES (17, 'ice', 'abc', 'picture1.png', '1', 1234, 3144, 4567, 1, 2);
INSERT INTO `food` VALUES (18, 'ice', 'abc', 'picture1.png', '1', 1234, 3144, 4567, 1, 2);
INSERT INTO `food` VALUES (19, 'ice', 'abc', 'picture1.png', '1', 1234, 3144, 4567, 1, 2);
INSERT INTO `food` VALUES (20, 'ice', 'abc', 'picture1.png', '1', 1234, 3144, 4567, 1, 2);
INSERT INTO `food` VALUES (21, 'ice', 'abc', 'picture1.png', '1', 1234, 3144, 4567, 1, 2);
INSERT INTO `food` VALUES (22, 'ice', 'abc', 'picture1.png', '1', 1234, 3144, 4567, 0, 2);
INSERT INTO `food` VALUES (23, 'ice', 'abc', 'picture1.png', '1', 1234, 3144, 4567, 0, 2);
INSERT INTO `food` VALUES (24, 'ice', 'abc', 'picture1.png', '1', 1234, 3144, 4567, 0, 2);
INSERT INTO `food` VALUES (25, 'ice123', 'abc', 'picture1.png', '1', 1234, 3144, 4567, 0, 1);
INSERT INTO `food` VALUES (26, 'ice', 'abc', 'picture1.png', '1', 1234, 3144, 4567, 0, 1);
INSERT INTO `food` VALUES (27, 'ice', 'abc', 'picture1.png', '1', 1234, 3144, 4567, 0, 1);
INSERT INTO `food` VALUES (28, 'ice', 'abc', 'picture1.png', '1', 1234, 3144, 4567, 1, 1);

-- ----------------------------
-- Table structure for food_censor
-- ----------------------------
DROP TABLE IF EXISTS `food_censor`;
CREATE TABLE `food_censor`  (
  `food_censor_id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `food_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `food_introduction` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `food_image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `food_category` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `food_price` float NULL DEFAULT NULL,
  `food_number` float NULL DEFAULT NULL,
  `food_calorie` float NULL DEFAULT NULL,
  `food_censor_create_date` datetime NULL DEFAULT NULL,
  `food_shop_id` int NULL DEFAULT NULL,
  `food_censor_status` int NULL DEFAULT NULL,
  `food_censor_msg` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `food_censor_type` int NULL DEFAULT NULL,
  PRIMARY KEY (`food_censor_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 42 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of food_censor
-- ----------------------------
INSERT INTO `food_censor` VALUES (33, 'ice', 'abc', 'picture1.png', '1', 1234, 3144, 4567, '2022-05-27 20:25:28', 1, 2, 'sadfasdfasdfdasfda', 0);
INSERT INTO `food_censor` VALUES (34, 'ice', 'abc', 'picture1.png', '1', 1234, 3144, 4567, '2022-05-27 21:56:48', 1, 0, NULL, 0);
INSERT INTO `food_censor` VALUES (35, 'ice', 'abc', 'picture1.png', '1', 1234, 3144, 4567, '2022-05-27 22:09:55', 1, 0, NULL, 0);
INSERT INTO `food_censor` VALUES (36, 'ice', 'abc', 'picture1.png', '1', 1234, 3144, 4567, '2022-05-28 15:07:04', 1, 0, NULL, 0);
INSERT INTO `food_censor` VALUES (37, 'ice', 'abc', 'picture1.png', '1', 1234, 3144, 4567, '2022-05-28 15:08:57', 1, 0, NULL, 0);
INSERT INTO `food_censor` VALUES (38, 'ice', 'abc', 'picture1.png', '1', 1234, 3144, 4567, '2022-05-28 15:12:15', 1, 0, NULL, 0);
INSERT INTO `food_censor` VALUES (39, 'ice', 'abc', 'picture1.png', '1', 1234, 3144, 4567, '2022-05-28 16:04:25', 1, 0, NULL, 0);
INSERT INTO `food_censor` VALUES (40, 'ice123', 'abc', 'picture1.png', NULL, 1234, 3144, 4567, '2022-05-28 16:19:14', 1, 0, NULL, 1);
INSERT INTO `food_censor` VALUES (41, 'ice123', 'abc', 'picture1.png', '1', 1234, 3144, 4567, '2022-05-28 16:20:29', 1, 0, NULL, 1);

-- ----------------------------
-- Table structure for food_order
-- ----------------------------
DROP TABLE IF EXISTS `food_order`;
CREATE TABLE `food_order`  (
  `food_id` int UNSIGNED NOT NULL,
  `order_id` int UNSIGNED NOT NULL,
  PRIMARY KEY (`order_id`, `food_id`) USING BTREE,
  INDEX `food_id`(`food_id`) USING BTREE,
  CONSTRAINT `food_order_ibfk_1` FOREIGN KEY (`food_id`) REFERENCES `food` (`food_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `food_order_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `order` (`order_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of food_order
-- ----------------------------

-- ----------------------------
-- Table structure for ingredient
-- ----------------------------
DROP TABLE IF EXISTS `ingredient`;
CREATE TABLE `ingredient`  (
  `ingredient_id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `ingredient_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `ingredient_category` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `ingredient_calorie` int NULL DEFAULT NULL,
  `ingredient_food_id` int NULL DEFAULT NULL,
  PRIMARY KEY (`ingredient_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ingredient
-- ----------------------------
INSERT INTO `ingredient` VALUES (1, 'eggs', 'egg', 10, 1);
INSERT INTO `ingredient` VALUES (2, 'banana', 'fruit', 11, 1);

-- ----------------------------
-- Table structure for ingredient_censor
-- ----------------------------
DROP TABLE IF EXISTS `ingredient_censor`;
CREATE TABLE `ingredient_censor`  (
  `ingredient_censor_id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `ingredient_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `ingredient_calorie` float NULL DEFAULT NULL,
  `ingredient_category` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `ingredient_censor_type` int NULL DEFAULT NULL,
  `ingredient_censor_food_censor_id` int NULL DEFAULT NULL,
  PRIMARY KEY (`ingredient_censor_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ingredient_censor
-- ----------------------------
INSERT INTO `ingredient_censor` VALUES (1, 'eggs', 10, 'egg', NULL, 37);
INSERT INTO `ingredient_censor` VALUES (2, 'banana', 11, 'fruit', NULL, 37);
INSERT INTO `ingredient_censor` VALUES (3, 'eggs', 10, 'egg', 0, 38);
INSERT INTO `ingredient_censor` VALUES (4, 'banana', 11, 'fruit', 0, 38);
INSERT INTO `ingredient_censor` VALUES (5, 'eggs', 10, 'egg', 0, 39);
INSERT INTO `ingredient_censor` VALUES (6, 'banana', 11, 'fruit', 0, 39);
INSERT INTO `ingredient_censor` VALUES (7, 'eggs', 10, 'egg', NULL, 40);
INSERT INTO `ingredient_censor` VALUES (8, 'banana', 11, 'fruit', NULL, 40);
INSERT INTO `ingredient_censor` VALUES (9, 'eggs', 10, 'egg', NULL, 41);
INSERT INTO `ingredient_censor` VALUES (10, 'banana', 11, 'fruit', NULL, 41);

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order`  (
  `order_id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_price` float NULL DEFAULT NULL,
  `order_calorie` float NULL DEFAULT NULL,
  `order_status` int NULL DEFAULT NULL,
  `order_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `order_rider_send` tinyint(1) NULL DEFAULT NULL,
  `order_commit` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`order_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order
-- ----------------------------

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `user_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `user_role` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'user',
  PRIMARY KEY (`user_name`, `user_role`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('user1', 'user');

-- ----------------------------
-- Table structure for service
-- ----------------------------
DROP TABLE IF EXISTS `service`;
CREATE TABLE `service`  (
  `service_id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `service_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `service_introduction` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `service_calorie` float NULL DEFAULT NULL,
  `service_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `service_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `service_shop_id` int NULL DEFAULT NULL,
  `service_category` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `service_image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`service_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of service
-- ----------------------------
INSERT INTO `service` VALUES (1, 'run', 'running', 200, '200', 'jiashi1', NULL, NULL, NULL);
INSERT INTO `service` VALUES (11, 'run', 'running', 200, '200', 'jiashi1', 2, NULL, NULL);
INSERT INTO `service` VALUES (12, 'skate', 'skating', 20, '20', 'jiashi', NULL, NULL, NULL);
INSERT INTO `service` VALUES (13, 'skate', 'skating', 20, '20', 'jiashi', NULL, NULL, NULL);
INSERT INTO `service` VALUES (14, 'skate', 'skating', 20, '20', 'jiashi', NULL, NULL, NULL);
INSERT INTO `service` VALUES (15, 'skate', 'skating', 20, '20', 'jiashi', NULL, NULL, NULL);
INSERT INTO `service` VALUES (16, 'skate', 'skating', 20, '20', 'jiashi', NULL, NULL, NULL);
INSERT INTO `service` VALUES (17, 'skate', 'skating', 20, '20', 'jiashi', NULL, NULL, NULL);
INSERT INTO `service` VALUES (18, 'skate', 'skating', 20, '20', 'jiashi', NULL, NULL, NULL);
INSERT INTO `service` VALUES (19, 'skate', 'skating', 20, '20', 'jiashi', NULL, NULL, NULL);
INSERT INTO `service` VALUES (20, 'skate', 'skating', 20, '20', 'jiashi', NULL, NULL, NULL);
INSERT INTO `service` VALUES (21, 'skate', 'skating', 20, '20', 'jiashi', 2, NULL, NULL);
INSERT INTO `service` VALUES (22, 'skate', 'skating', 20, '20', 'jiashi', 2, NULL, NULL);

-- ----------------------------
-- Table structure for service_censor
-- ----------------------------
DROP TABLE IF EXISTS `service_censor`;
CREATE TABLE `service_censor`  (
  `service_id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `service_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `service_introduction` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `service_calorie` float NULL DEFAULT NULL,
  `service_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `service_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `service_censor_category` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `service_shop_id` int NULL DEFAULT NULL,
  `service_create_date` datetime NULL DEFAULT NULL,
  `service_censor_state` int NULL DEFAULT NULL,
  `service_censor_msg` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `service_censor_type` int NULL DEFAULT NULL,
  `service_image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`service_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 29 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of service_censor
-- ----------------------------
INSERT INTO `service_censor` VALUES (25, 'skate', 'skating', 20, '20', 'jiashi', NULL, 2, '2022-05-27 21:35:46', 2, 'sadfasdfasdfdasfda', 0, NULL);
INSERT INTO `service_censor` VALUES (27, 'skate', 'skating', 20, '20', 'jiashi', NULL, 2, '2022-05-27 22:23:36', 2, 'sadfasdfasdfdasfda', 0, NULL);
INSERT INTO `service_censor` VALUES (28, 'run', 'running', 200, '200', 'jiashi1', NULL, 2, '2022-05-27 22:23:42', 0, NULL, 11, NULL);

-- ----------------------------
-- Table structure for shop
-- ----------------------------
DROP TABLE IF EXISTS `shop`;
CREATE TABLE `shop`  (
  `shop_id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `shop_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `shop_introduction` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `shop_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `shop_image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `shop_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`shop_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of shop
-- ----------------------------
INSERT INTO `shop` VALUES (1, 'Mc', 'western style', 'jiashi', 'pictures', 'food');
INSERT INTO `shop` VALUES (2, 'workout', 'swimming pool', 'jiading', 'pictures', 'workout');

-- ----------------------------
-- Table structure for shop_order
-- ----------------------------
DROP TABLE IF EXISTS `shop_order`;
CREATE TABLE `shop_order`  (
  `shop_id` int UNSIGNED NULL DEFAULT NULL,
  `order_id` int UNSIGNED NOT NULL,
  PRIMARY KEY (`order_id`) USING BTREE,
  INDEX `shop_id`(`shop_id`) USING BTREE,
  CONSTRAINT `shop_order_ibfk_1` FOREIGN KEY (`shop_id`) REFERENCES `shop` (`shop_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `shop_order_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `order` (`order_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of shop_order
-- ----------------------------

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `user_id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `user_password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `user_nickname` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `user_phone` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `user_email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `user_height` float NULL DEFAULT NULL,
  `user_weight` float NULL DEFAULT NULL,
  `user_age` int NULL DEFAULT NULL,
  `user_sex` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `user_bmi` float NULL DEFAULT NULL,
  `user_ree` float NULL DEFAULT NULL,
  `user_is_food_merchant` int NULL DEFAULT 0,
  `user_is_gym_merchant` int NULL DEFAULT 0,
  `user_is_rider` int NULL DEFAULT 0,
  `user_drivinglicense` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `user_health_certificate` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `user_qualification_proof` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `user_id_proof` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `user_role` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT 'user',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (14, 'user1', 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, '3275284157@qq.com', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, NULL, NULL, 'blob:http://localhost:8080/83536e40-db44-4fbb-b1c5-fd4ca4fbeef2', 'blob:http://localhost:8080/876bad34-200e-43cd-85d2-558d466066ec', 'user');

-- ----------------------------
-- Table structure for user_order
-- ----------------------------
DROP TABLE IF EXISTS `user_order`;
CREATE TABLE `user_order`  (
  `user_id` int UNSIGNED NULL DEFAULT NULL,
  `order_id` int UNSIGNED NOT NULL,
  PRIMARY KEY (`order_id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  CONSTRAINT `user_order_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `user_order_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `order` (`order_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order
-- ----------------------------

-- ----------------------------
-- Table structure for user_shop
-- ----------------------------
DROP TABLE IF EXISTS `user_shop`;
CREATE TABLE `user_shop`  (
  `user_id` int UNSIGNED NULL DEFAULT NULL,
  `shop_id` int UNSIGNED NOT NULL,
  PRIMARY KEY (`shop_id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  CONSTRAINT `user_shop_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `user_shop_ibfk_2` FOREIGN KEY (`shop_id`) REFERENCES `shop` (`shop_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_shop
-- ----------------------------
INSERT INTO `user_shop` VALUES (14, 1);
INSERT INTO `user_shop` VALUES (14, 2);

SET FOREIGN_KEY_CHECKS = 1;
