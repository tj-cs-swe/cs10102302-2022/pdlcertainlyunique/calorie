/*
 Navicat Premium Data Transfer

 Source Server         : root
 Source Server Type    : MySQL
 Source Server Version : 80026
 Source Host           : localhost:3306
 Source Schema         : calorie

 Target Server Type    : MySQL
 Target Server Version : 80026
 File Encoding         : 65001

 Date: 30/05/2022 22:22:24
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for food
-- ----------------------------
DROP TABLE IF EXISTS `food`;
CREATE TABLE `food`  (
  `food_id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `food_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `food_introduction` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `food_image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `food_category` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `food_ingredient` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `food_price` float NULL DEFAULT NULL,
  `food_number` float NULL DEFAULT NULL,
  `food_calorie` float NULL DEFAULT NULL,
  `food_take_status` int NULL DEFAULT NULL,
  `food_shop_id` int NULL DEFAULT NULL,
  PRIMARY KEY (`food_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of food
-- ----------------------------
INSERT INTO `food` VALUES (1, 'name', 'introduction', 'image', 'category', 'ingredient', 10, 100, 20.5, 1, 1);
INSERT INTO `food` VALUES (11, 'ice', 'abc', 'picture1.png', '1', 'ingredient', 1234, 3144, 4567, 1, 2);
INSERT INTO `food` VALUES (12, 'ice', 'abc', 'picture1.png', '1', 'ingredient', 1234, 3144, 4567, 1, 2);
INSERT INTO `food` VALUES (13, 'ice', 'abc', 'picture1.png', '1', 'ingredient', 1234, 3144, 4567, 1, 2);
INSERT INTO `food` VALUES (14, 'ice', 'abc', 'picture1.png', '1', 'ingredient', 1234, 3144, 4567, 1, 2);
INSERT INTO `food` VALUES (15, 'ice', 'abc', 'picture1.png', '1', 'ingredient', 1234, 3144, 4567, 1, 2);
INSERT INTO `food` VALUES (16, 'ice', 'abc', 'picture1.png', '1', 'ingredient', 1234, 3144, 4567, 1, 2);
INSERT INTO `food` VALUES (17, 'ice', 'abc', 'picture1.png', '1', 'ingredient', 1234, 3144, 4567, 1, 2);
INSERT INTO `food` VALUES (18, 'ice', 'abc', 'picture1.png', '1', 'ingredient', 1234, 3144, 4567, 1, 2);
INSERT INTO `food` VALUES (19, 'ice', 'abc', 'picture1.png', '1', 'ingredient', 1234, 3144, 4567, 1, 2);
INSERT INTO `food` VALUES (20, 'ice', 'abc', 'picture1.png', '1', 'ingredient', 1234, 3144, 4567, 1, 2);
INSERT INTO `food` VALUES (21, 'ice', 'abc', 'picture1.png', '1', 'ingredient', 1234, 3144, 4567, 1, 2);
INSERT INTO `food` VALUES (22, 'ice', 'abc', 'picture1.png', '1', 'ingredient', 1234, 3144, 4567, 0, 2);
INSERT INTO `food` VALUES (23, 'ice', 'abc', 'picture1.png', '1', 'ingredient', 1234, 3144, 4567, 0, 2);
INSERT INTO `food` VALUES (24, 'ice', 'abc', 'picture1.png', '1', 'ingredient', 1234, 3144, 4567, 0, 2);
INSERT INTO `food` VALUES (25, 'ice123', 'abc', 'picture1.png', '1', 'ingredient', 1234, 3144, 4567, 0, 1);
INSERT INTO `food` VALUES (26, 'ice', 'abc', 'picture1.png', '1', 'ingredient', 1234, 3144, 4567, 0, 1);
INSERT INTO `food` VALUES (27, 'ice', 'abc', 'picture1.png', '1', 'ingredient', 1234, 3144, 4567, 0, 1);
INSERT INTO `food` VALUES (28, 'ice', 'abc', 'picture1.png', '1', 'ingredient', 1234, 3144, 4567, 1, 1);
INSERT INTO `food` VALUES (29, 'name', 'introduction', 'image', 'category', 'ingredient', 10, 100, 20.5, 1, 1);

-- ----------------------------
-- Table structure for food_censor
-- ----------------------------
DROP TABLE IF EXISTS `food_censor`;
CREATE TABLE `food_censor`  (
  `food_censor_id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `food_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `food_introduction` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `food_image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `food_category` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `food_price` float NULL DEFAULT NULL,
  `food_number` float NULL DEFAULT NULL,
  `food_calorie` float NULL DEFAULT NULL,
  `food_censor_create_date` datetime NULL DEFAULT NULL,
  `food_shop_id` int NULL DEFAULT NULL,
  `food_censor_status` int NULL DEFAULT NULL,
  `food_censor_msg` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `food_censor_type` int NULL DEFAULT NULL,
  `food_ingredient` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`food_censor_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 43 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of food_censor
-- ----------------------------
INSERT INTO `food_censor` VALUES (33, 'ice', 'abc', 'picture1.png', '1', 1234, 3144, 4567, '2022-05-27 20:25:28', 1, 2, 'sadfasdfasdfdasfda', 0, 'ingredient');
INSERT INTO `food_censor` VALUES (34, 'ice', 'abc', 'picture1.png', '1', 1234, 3144, 4567, '2022-05-27 21:56:48', 1, 0, NULL, 0, 'ingredient');
INSERT INTO `food_censor` VALUES (35, 'ice', 'abc', 'picture1.png', '1', 1234, 3144, 4567, '2022-05-27 22:09:55', 1, 2, 'sadfasdfasdfdasfda', 0, 'ingredient');
INSERT INTO `food_censor` VALUES (36, 'ice', 'abc', 'picture1.png', '1', 1234, 3144, 4567, '2022-05-28 15:07:04', 1, 0, NULL, 0, 'ingredient');
INSERT INTO `food_censor` VALUES (37, 'ice', 'abc', 'picture1.png', '1', 1234, 3144, 4567, '2022-05-28 15:08:57', 1, 0, NULL, 0, 'ingredient');
INSERT INTO `food_censor` VALUES (38, 'ice', 'abc', 'picture1.png', '1', 1234, 3144, 4567, '2022-05-28 15:12:15', 1, 0, NULL, 0, 'ingredient');
INSERT INTO `food_censor` VALUES (39, 'ice', 'abc', 'picture1.png', '1', 1234, 3144, 4567, '2022-05-28 16:04:25', 1, 0, NULL, 0, 'ingredient');
INSERT INTO `food_censor` VALUES (40, 'ice123', 'abc', 'picture1.png', NULL, 1234, 3144, 4567, '2022-05-28 16:19:14', 1, 0, NULL, 1, 'ingredient');
INSERT INTO `food_censor` VALUES (41, 'ice123', 'abc', 'picture1.png', '1', 1234, 3144, 4567, '2022-05-28 16:20:29', 1, 0, NULL, 1, 'ingredient');

-- ----------------------------
-- Table structure for food_foodorder
-- ----------------------------
DROP TABLE IF EXISTS `food_foodorder`;
CREATE TABLE `food_foodorder`  (
  `food_foodorder_food_id` int UNSIGNED NOT NULL,
  `food_foodorder_order_id` int UNSIGNED NOT NULL,
  `food_foodorder_food_number` int NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of food_foodorder
-- ----------------------------
INSERT INTO `food_foodorder` VALUES (11, 1, 12);
INSERT INTO `food_foodorder` VALUES (12, 1, 11);

-- ----------------------------
-- Table structure for foodorder
-- ----------------------------
DROP TABLE IF EXISTS `foodorder`;
CREATE TABLE `foodorder`  (
  `food_order_id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `food_order_price` float NULL DEFAULT NULL,
  `food_order_calorie` float NULL DEFAULT NULL,
  `food_order_status` int NULL DEFAULT NULL,
  `food_order_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `food_order_rider_send` tinyint(1) NULL DEFAULT NULL,
  `food_order_comment` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `food_order_shop_id` int NULL DEFAULT NULL,
  `food_order_user_id` int NULL DEFAULT NULL,
  PRIMARY KEY (`food_order_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of foodorder
-- ----------------------------
INSERT INTO `foodorder` VALUES (1, 12, 10.5, 1, 'location', 1, '?', 1, 14);

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `user_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `user_role` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'user',
  PRIMARY KEY (`user_name`, `user_role`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('admin', 'admin');
INSERT INTO `role` VALUES ('admin', 'user');
INSERT INTO `role` VALUES ('user1', 'user');

-- ----------------------------
-- Table structure for service
-- ----------------------------
DROP TABLE IF EXISTS `service`;
CREATE TABLE `service`  (
  `service_id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `service_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `service_introduction` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `service_calorie` float NULL DEFAULT NULL,
  `service_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `service_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `service_shop_id` int NULL DEFAULT NULL,
  `service_category` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `service_image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`service_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of service
-- ----------------------------
INSERT INTO `service` VALUES (1, 'run', 'running', 200, '200', 'jiashi1', NULL, NULL, NULL);
INSERT INTO `service` VALUES (11, 'name', 'introduction', 3.14, '8:00-20:00', 'location', 2, 'category', 'image');
INSERT INTO `service` VALUES (12, 'skate', 'skating', 20, '20', 'jiashi', NULL, NULL, NULL);
INSERT INTO `service` VALUES (13, 'skate', 'skating', 20, '20', 'jiashi', NULL, NULL, NULL);
INSERT INTO `service` VALUES (14, 'skate', 'skating', 20, '20', 'jiashi', NULL, NULL, NULL);
INSERT INTO `service` VALUES (15, 'skate', 'skating', 20, '20', 'jiashi', NULL, NULL, NULL);
INSERT INTO `service` VALUES (16, 'skate', 'skating', 20, '20', 'jiashi', NULL, NULL, NULL);
INSERT INTO `service` VALUES (17, 'skate', 'skating', 20, '20', 'jiashi', NULL, NULL, NULL);
INSERT INTO `service` VALUES (18, 'skate', 'skating', 20, '20', 'jiashi', NULL, NULL, NULL);
INSERT INTO `service` VALUES (19, 'skate', 'skating', 20, '20', 'jiashi', NULL, NULL, NULL);
INSERT INTO `service` VALUES (20, 'skate', 'skating', 20, '20', 'jiashi', NULL, NULL, NULL);
INSERT INTO `service` VALUES (21, 'skate', 'skating', 20, '20', 'jiashi', 2, NULL, NULL);
INSERT INTO `service` VALUES (22, 'skate', 'skating', 20, '20', 'jiashi', 2, NULL, NULL);
INSERT INTO `service` VALUES (23, 'name', 'introduction', 3.14, '8:00-20:00', 'location', 2, 'category', 'image');

-- ----------------------------
-- Table structure for service_censor
-- ----------------------------
DROP TABLE IF EXISTS `service_censor`;
CREATE TABLE `service_censor`  (
  `service_censor_id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `service_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `service_introduction` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `service_calorie` float NULL DEFAULT NULL,
  `service_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `service_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `service_category` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `service_shop_id` int NULL DEFAULT NULL,
  `service_image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `service_censor_create_date` datetime NULL DEFAULT NULL,
  `service_censor_status` int NULL DEFAULT NULL,
  `service_censor_msg` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `service_censor_type` int NULL DEFAULT NULL,
  PRIMARY KEY (`service_censor_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 32 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of service_censor
-- ----------------------------
INSERT INTO `service_censor` VALUES (25, 'skate', 'skating', 20, '20', 'jiashi', NULL, 2, NULL, '2022-05-27 21:35:46', 2, 'sadfasdfasdfdasfda', 0);
INSERT INTO `service_censor` VALUES (27, 'skate', 'skating', 20, '20', 'jiashi', NULL, 2, NULL, '2022-05-27 22:23:36', 2, 'sadfasdfasdfdasfda', 0);
INSERT INTO `service_censor` VALUES (28, 'run', 'running', 200, '200', 'jiashi1', 'category', 2, 'image', '2022-05-27 22:23:42', 0, NULL, 11);
INSERT INTO `service_censor` VALUES (29, 'name', 'introduction', 3.14, '8:00-20:00', 'location', 'category', 2, 'image', '2022-05-29 02:15:02', 0, NULL, 0);
INSERT INTO `service_censor` VALUES (30, 'name', 'introduction', 3.14, '8:00-20:00', 'location', NULL, 2, 'image', '2022-05-29 02:15:51', 2, 'sadfasdfasdfdasfda', 11);

-- ----------------------------
-- Table structure for service_serviceorder
-- ----------------------------
DROP TABLE IF EXISTS `service_serviceorder`;
CREATE TABLE `service_serviceorder`  (
  `service_id` int NOT NULL,
  `service_order_id` int NULL DEFAULT NULL,
  PRIMARY KEY (`service_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of service_serviceorder
-- ----------------------------
INSERT INTO `service_serviceorder` VALUES (11, 1);
INSERT INTO `service_serviceorder` VALUES (12, 1);

-- ----------------------------
-- Table structure for serviceorder
-- ----------------------------
DROP TABLE IF EXISTS `serviceorder`;
CREATE TABLE `serviceorder`  (
  `service_order_id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `service_order_arrival_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `service_order_status` int NULL DEFAULT NULL,
  `service_order_comment` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `service_order_shop_id` int NULL DEFAULT NULL,
  `service_order_user_id` int NULL DEFAULT NULL,
  PRIMARY KEY (`service_order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of serviceorder
-- ----------------------------
INSERT INTO `serviceorder` VALUES (1, 'arrival time', 2, '?', 2, 14);

-- ----------------------------
-- Table structure for shop
-- ----------------------------
DROP TABLE IF EXISTS `shop`;
CREATE TABLE `shop`  (
  `shop_id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `shop_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `shop_introduction` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `shop_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `shop_image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `shop_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`shop_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of shop
-- ----------------------------
INSERT INTO `shop` VALUES (1, 'Mc', 'western style', 'jiashi', 'pictures', 'food');
INSERT INTO `shop` VALUES (2, 'workout', 'swimming pool', 'jiading', 'pictures', 'workout');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `user_id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `user_password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `user_nickname` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `user_phone` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `user_email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `user_height` float NULL DEFAULT NULL,
  `user_weight` float NULL DEFAULT NULL,
  `user_age` int NULL DEFAULT NULL,
  `user_sex` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `user_bmi` float NULL DEFAULT NULL,
  `user_ree` float NULL DEFAULT NULL,
  `user_is_food_merchant` int NULL DEFAULT 0,
  `user_is_gym_merchant` int NULL DEFAULT 0,
  `user_is_rider` int NULL DEFAULT 0,
  `user_drivinglicense` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `user_health_certificate` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `user_qualification_proof` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `user_id_proof` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `user_role` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT 'user',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'admin', 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 'user');
INSERT INTO `user` VALUES (14, 'user1', 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, '3275284157@qq.com', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, NULL, NULL, 'blob:http://localhost:8080/83536e40-db44-4fbb-b1c5-fd4ca4fbeef2', 'blob:http://localhost:8080/876bad34-200e-43cd-85d2-558d466066ec', 'user');

-- ----------------------------
-- Table structure for user_shop
-- ----------------------------
DROP TABLE IF EXISTS `user_shop`;
CREATE TABLE `user_shop`  (
  `user_id` int UNSIGNED NULL DEFAULT NULL,
  `shop_id` int UNSIGNED NOT NULL,
  PRIMARY KEY (`shop_id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  CONSTRAINT `user_shop_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `user_shop_ibfk_2` FOREIGN KEY (`shop_id`) REFERENCES `shop` (`shop_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_shop
-- ----------------------------
INSERT INTO `user_shop` VALUES (14, 1);
INSERT INTO `user_shop` VALUES (14, 2);

SET FOREIGN_KEY_CHECKS = 1;
