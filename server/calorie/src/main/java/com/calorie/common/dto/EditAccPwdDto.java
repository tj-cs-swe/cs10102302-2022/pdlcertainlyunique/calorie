package com.calorie.common.dto;

import lombok.Data;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
public class EditAccPwdDto implements Serializable
{
    @NotBlank(message = "账号id不能为空")
    private Integer userId;
    @NotBlank(message = "新密码不能为空")
    private String password;
}
