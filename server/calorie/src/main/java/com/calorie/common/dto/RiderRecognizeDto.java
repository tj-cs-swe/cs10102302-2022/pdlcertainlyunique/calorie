package com.calorie.common.dto;

import lombok.Data;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
public class RiderRecognizeDto implements Serializable{
    @NotBlank(message = "用户名不能为空")
    private String userName;

    @NotBlank(message = "驾驶证证明不能为空")
    private String userDrivingLicense;

    @NotBlank(message = "健康证明不能为空")
    private String userHealthCertificate;
}

