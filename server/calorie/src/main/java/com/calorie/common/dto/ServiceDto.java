package com.calorie.common.dto;

import lombok.Data;

import java.util.Date;

@Data
public class ServiceDto {
    private Integer serviceId;

    private String serviceName;

    private String serviceIntroduction;

    private String serviceCalorie;

    private String serviceTime;

    private String serviceLocation;

    private Date serviceCreateDate;

    private Integer serviceShopId;

    private Integer serviceCensorState;

    private Integer serviceCensorType;

    private String serviceCensorMsg;

    private String serviceCreateDateFormat;
}
