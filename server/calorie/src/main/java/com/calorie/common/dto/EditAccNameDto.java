package com.calorie.common.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
public class EditAccNameDto implements Serializable {
    public enum editType {
        editNickName,
        editUserName
    }

    private String name;
    @NotBlank(message = "账号id不能为空")
    private Integer userId;
    private editType et;
}
