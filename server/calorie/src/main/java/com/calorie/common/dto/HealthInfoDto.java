package com.calorie.common.dto;

import lombok.Data;

@Data
public class HealthInfoDto {
    private float userHeight;
    private float userWeight;
    private int userAge;
    private String userSex;
}
