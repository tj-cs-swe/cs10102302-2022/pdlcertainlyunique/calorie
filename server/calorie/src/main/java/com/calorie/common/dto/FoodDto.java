package com.calorie.common.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.Date;

@Data
public class FoodDto {
    private Integer foodId;

    @NotBlank(message = "食物名不能为空")
    private String foodName;

    private String foodIntroduction;

    private String foodImage;

    private String foodType;

    private String foodIngredient;

    @NotBlank(message = "价格不能为空")
    private Float foodPrice;

    private Float foodNumber;

    @NotBlank(message = "卡路里不能为空")
    private Float foodCalorie;

    private Integer foodStatus;

    private Date foodCreateDate;

    private Integer foodShopId;

    private Integer foodCensorState;

    private Integer foodCensorType;

    private String foodCensorMsg;

    private String foodCreateDateFormat;
}
