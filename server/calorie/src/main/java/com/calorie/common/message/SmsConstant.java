package com.calorie.common.message;

public final class SmsConstant {
    //SDK APP_ID
    public static final int APP_ID = 1400664950 ;
    //SDK APP_KEY
    public static final String APP_KEY="5954262a06d407741ec2dfcda4155e1f";
    //模板ID,如果需要多个模板可更改为可变参数
    public static final int TEMPLATE_ID = 1373893 ;
    //签名
    public static final String SIGN = "springbootms";
    //验证码存储在redis的时间
    public static final Integer EFFECTIVE_Time = 300;
}

