package com.calorie.common.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
public class AuthAuditDto implements Serializable{
    @NotBlank(message = "用户名不能为空")
    @ApiModelProperty(value = "用户名")
    private String userName;

    @NotBlank(message = "审核是否通过不能为空")
    @ApiModelProperty(value = "审核是否通过")
    private int isVerified;
}

