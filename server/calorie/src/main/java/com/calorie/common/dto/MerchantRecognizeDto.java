package com.calorie.common.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
public class MerchantRecognizeDto implements Serializable {
    @NotBlank(message = "用户名不能为空")
    private String userName;

    @NotBlank(message = "资质证明不能为空")
    private String userQualificationProof;

    @NotBlank(message = "身份证明不能为空")
    private String userIdProof;
}
