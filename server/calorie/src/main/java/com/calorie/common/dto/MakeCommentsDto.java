package com.calorie.common.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class MakeCommentsDto implements Serializable
{
    Integer userId;
    String comment;
}
