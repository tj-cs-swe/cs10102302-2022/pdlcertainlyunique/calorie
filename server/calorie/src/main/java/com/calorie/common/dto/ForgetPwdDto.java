package com.calorie.common.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
public class ForgetPwdDto implements Serializable
{
    @NotBlank(message = "用户名不能为空")
    private String userName;
    private Integer type;
    @NotBlank(message = "邮箱或手机号不能为空")
    private String connection;
    private String status;
    private String code;
    @NotBlank(message = "密码不能为空")
    private String password;
}
