package com.calorie.common.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class FoodOrderDto implements Serializable
{
    Integer userId;
    Integer shopId;
    List<List<Integer>> foodIdAndNum;
    String foodOrderLocation;
}
