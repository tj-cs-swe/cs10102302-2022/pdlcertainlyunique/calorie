package com.calorie.common.dto;

import lombok.Data;

@Data
public class UserIdDto {
    private Integer userId;
}
