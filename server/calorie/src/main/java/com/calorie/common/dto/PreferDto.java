package com.calorie.common.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class PreferDto {
    @NotBlank(message = "用户喜好不能为空")
    @ApiModelProperty(value = "用户喜好")
    private String prefer;

    @ApiModelProperty(value = "用户活动量偏好")
    private int userWorkoutQuantityPrefer;
}
