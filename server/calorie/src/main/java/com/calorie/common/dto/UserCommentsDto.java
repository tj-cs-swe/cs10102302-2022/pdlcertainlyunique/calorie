package com.calorie.common.dto;

import lombok.Data;

@Data
public class UserCommentsDto
{
    String comment;
    String userName;
    String userNickname;
}
