package com.calorie.common.dto;

import lombok.Data;

@Data
public class OrderUserDto {
    private String userId;

    private String userName;

    private String userNickname;

    private String userPhone;

    private String userEmail;
}
