package com.calorie.realm;

import com.calorie.model.User;
import com.calorie.service.account.UserService;
import com.calorie.shiro.JwtToken;
import com.calorie.util.JwtUtil;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AccountRealm extends AuthorizingRealm {
    @Autowired
    private UserService userService;
    /*
     * 多重写一个support
     * 标识这个Realm是专门用来验证JwtToken
     * 不负责验证其他的token（UsernamePasswordToken）
     * */
    @Override
    public boolean supports(AuthenticationToken token) {
        //这个token就是从过滤器中传入的jwtToken
        return token instanceof JwtToken;
    }

    //授权
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        String token= principals.toString();
        JwtUtil jwtUtil = new JwtUtil();
        String username = (String) jwtUtil.decode(token).get("username");

        User user = userService.selectByUserName(username);

        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();

        List<String> roles = userService.selectRolesByUserName(user.getUserName());

        roles.forEach((role) -> {
            // 用户角色
            info.addRole(role);
            System.out.println(role);
        });

        // 查询数据库来获取用户的权限
        // info.addStringPermission(user.getPermission());
        return info;
    }

    //认证
    //这个token就是从过滤器中传入的jwtToken
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {

        String jwt = (String) token.getPrincipal();
        String username = null;
        JwtUtil jwtUtil = new JwtUtil();
        try {
            username = (String) jwtUtil.decode(jwt).get("username");
        } catch (Exception e){
            throw new AuthenticationException("token invalid1");
        }

        if (!jwtUtil.isVerify(jwt) || username == null){
            throw new AuthenticationException("token invalid2");
        }

        User user = userService.selectByUserName(username);

        if (user == null){
            throw new AuthenticationException("user null");
        }

        return new SimpleAuthenticationInfo(jwt, jwt,"AccountRealm");
        //这里返回的是类似账号密码的东西，但是jwtToken都是jwt字符串。还需要一个该Realm的类名

    }

}