package com.calorie.mapper.food;

import com.calorie.model.FoodCensor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FoodCensorMapper {
    int insertNewFoodForCensor(FoodCensor foodCensor);
//    int insertIngredientCensor(List<IngredientCensor> ingredientList);

    List<FoodCensor> selectAllFoodCensor();

    FoodCensor selectFoodCensorByFoodId(Integer foodCensorId);

    int deleteFoodCensorByFoodCensorId(Integer foodCensorId);

    int updateRejectFoodMsg(FoodCensor foodCensor);
}