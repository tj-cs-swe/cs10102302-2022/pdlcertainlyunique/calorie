package com.calorie.mapper.order;

import com.calorie.common.dto.UserCommentsDto;
import com.calorie.model.FoodOrder;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FoodOrderMapper {
    List<FoodOrder> selectAllFoodOrderByShopId(Integer shopId);


    List<FoodOrder> selectFoodOrderByShopIdFoodOrderStatus(Integer shopId, Integer foodOrderStatus);

    FoodOrder selectFoodOrderByFoodOrderId(Integer foodOrderId);

    int updateFoodOrderStatus(Integer foodOrderId);



    List<Integer> queryFoodOrderAssignedToRider(Integer riderUserId);
    List<FoodOrder> queryFoodOrderByUserId(Integer userId);

    List<FoodOrder> queryFoodOrderByUserIdFoodOrderStatus(Integer userId, Integer foodOrderStatus);

    void insertFoodOrder(FoodOrder foodOrder);

    void insertFoodFoodOrder(Integer foodId, Integer foodNumber, Integer foodOrderId);

    Float getFoodCalorie(Integer foodId);

    Float getFoodPrice(Integer foodId);

    Integer getCurFoodOrderId();

    String queryCommentByFoodOrderId(Integer foodOrderId);

    List<UserCommentsDto> queryCommentsByShopId(Integer shopId);

    void confirmFoodOrder(Integer foodOrderId, Integer nextStatus);

    FoodOrder queryFoodOrderByFoodOrderId(Integer foodOrderId);

    FoodOrder queryDetailedFoodOrderByFoodOrderId(Integer foodOrderId);

    void updateComment(Integer foodOrderId, String comment);

    void updateFoodOrderRiderSend(Integer foodOrderId, Integer sendOrder);

    void insertFoodOrderRider(Integer foodOrderId, Integer riderUserId);

    void updateFoodOrderRider(Integer foodOrderId, Integer riderUserId);

    Integer queryFoodOrderRider(Integer foodOrderId);

    List<Integer> getAllRidersUserId();
}
