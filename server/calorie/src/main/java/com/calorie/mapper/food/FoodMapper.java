package com.calorie.mapper.food;

import com.calorie.model.Food;
import com.calorie.model.FoodCensor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FoodMapper {
    List<Food> selectFoodByShopIdStatus(Integer shopId);

    List<Food> selectAllFoodByShopId(Integer shopId);

    List<Food> selectAllFood();

    Food selectFoodByFoodIdShopId(Integer foodId, Integer shopId);

    int insertNewFood(FoodCensor food);

    int updateFoodByFoodId(FoodCensor food);

    int updateFoodTakeStatus(Integer foodTakeStatus, Integer foodId);


}
