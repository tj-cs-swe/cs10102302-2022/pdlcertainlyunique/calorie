package com.calorie.mapper.account;

import com.calorie.common.dto.RegisterDto;
import com.calorie.model.User;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserMapper {
    User selectByUserName(String userName);

    int insertUser(RegisterDto registerDto);

    int insertUserRole(String userName);

    User selectByUserPhone(String userPhone);

    User selectByUserEmail(String userEmail);

    List<String> selectRolesByUserName(String userName);

    List<String> selectServicePreferByUserName(String userName);

    List<String> selectFoodPreferByUserName(String userName);

    User selectUserByShopId(Integer shopId);
}
