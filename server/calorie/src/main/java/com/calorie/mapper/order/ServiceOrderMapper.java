package com.calorie.mapper.order;

import com.calorie.model.ServiceOrder;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ServiceOrderMapper {
    List<ServiceOrder> selectServiceOrderByShopIdServiceOrderStatus(Integer shopId, Integer serviceOrderStatus);

    ServiceOrder selectServiceOrderByServiceOrderId(Integer serviceOrderId);

    int updateServiceOrderStatus(Integer serviceOrderId);

    List<ServiceOrder> selectAllServiceOrderByShopId(Integer shopId);

    void insertServiceOrder(ServiceOrder serviceOrder);

    void insertServiceServiceOrder(Integer serviceId, Integer ServiceOrderId, Integer serviceAppointmentTime);
    Integer getCurServiceOrderId();
    void changeServiceOrderStatus(Integer serviceOrderId, Integer nextStatus);
    void updateServiceOrderComment(Integer serviceOrderId, String comment);

    List<ServiceOrder> selectAllServiceOrderByUserId(Integer userId);

    Integer selectServiceServiceOrderAppointmentTime(Integer serviceId, Integer serviceOrderId);
}