package com.calorie.mapper.workout;

import com.calorie.model.ServiceCensor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ServiceCensorMapper {
    int insertNewServiceForCensor(ServiceCensor serviceCensor);

    List<ServiceCensor> selectAllServiceCensor();

    ServiceCensor selectServiceCensorByServiceId(Integer serviceCensorId);

    int updateRejectServiceMsg(ServiceCensor serviceCensor);

}
