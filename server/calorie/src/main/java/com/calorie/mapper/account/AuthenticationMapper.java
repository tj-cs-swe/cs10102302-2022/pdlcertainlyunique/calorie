package com.calorie.mapper.account;

import com.calorie.common.dto.MerchantRecognizeDto;
import com.calorie.common.dto.RiderRecognizeDto;
import com.calorie.model.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AuthenticationMapper {
    int updateRiderAuthInfo(@Param("userIsRider")int userIsRider,
                            @Param("riderDto")RiderRecognizeDto riderDto);
    int updateUserIsRider(@Param("userIsRider")int userIsRider,
                          @Param("userName")String userName);

    int updateFoodMerchantAuthInfo(@Param("userIsFoodMerchant")int userIsFoodMerchant,
                                   @Param("merchantDto") MerchantRecognizeDto merchantDto);
    int updateUserIsFoodMerchant(@Param("userIsFoodMerchant")int userIsFoodMerchant,
                                 @Param("userName")String userName);

    int updateGymMerchantAuthInfo(@Param("userIsGymMerchant")int userIsGymMerchant,
                                  @Param("merchantDto") MerchantRecognizeDto merchantDto);
    int updateUserIsGymMerchant(@Param("userIsGymMerchant")int userIsGymMerchant,
                                @Param("userName")String userName);

    List<User> selectRiderAuthRequests();
    List<User> selectFoodMerchantAuthRequests();
    List<User> selectGymMerchantAuthRequests();

    void addUserRole(@Param("userName") String userName, @Param("userRole")String userRole);
    void deleteUserRole(@Param("userName") String userName, @Param("userRole")String userRole);
}
