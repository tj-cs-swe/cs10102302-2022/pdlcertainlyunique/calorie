package com.calorie.mapper.workout;

import com.calorie.common.dto.ServiceDto;
import com.calorie.model.Service;
import com.calorie.model.ServiceCensor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ServiceMapper {
    List<Service> selectServiceByShopId(Integer shopId);

    List<Service> selectAllService();

    int insertNewService(ServiceCensor service);

    int insertServiceShopId(Integer serviceId, Integer shopId);

    int updateServiceByServiceId(ServiceCensor serviceCensor);

    Service selectServiceByServiceIdShopId(Integer serviceId, Integer shopId);

    int insertNewServiceForCensor(ServiceCensor serviceCensor);

    ServiceDto selectNewServiceByServiceId(Integer serviceId);

    int deleteNewService(Integer serviceId);

    Service selectServiceByServiceId(Integer serviceId);

}