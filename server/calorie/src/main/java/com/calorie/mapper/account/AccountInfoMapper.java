package com.calorie.mapper.account;

import com.calorie.model.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountInfoMapper {
    User selectByUserId(Integer userId);

    Integer updateUserNickname(String userNickname, Integer userId);

    Integer updateUserName(String userName, Integer userId);

    Integer updateUserPassword(String password, Integer userId);

    // 以下为用户偏好设置
    void addUserFoodTypePrefer(String userName, String prefer);

    void deleteUserFoodTypePrefer(String userName, String prefer);

    List<String> selectUserFoodTypePreferByUserName(String userName);

    void addUserServiceTypePrefer(String userName, String prefer);

    void deleteUserServiceTypePrefer(String userName, String prefer);

    List<String> selectUserServiceTypePreferByUsernName(String userName);

    void updateWorkoutQuantityPreferByUserName(String userName, Integer quantity);
    // 以上为用户偏好设置

    void updateUserHealthInfo(@Param("user") User user);

    void updateChangeNameRole(String oldname, String newname);
}