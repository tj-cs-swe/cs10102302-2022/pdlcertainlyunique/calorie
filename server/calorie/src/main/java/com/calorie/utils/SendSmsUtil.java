package com.calorie.utils;

import cn.hutool.json.JSONException;
import com.calorie.common.message.SmsConstant;
import com.github.qcloudsms.SmsSingleSender;
import com.github.qcloudsms.SmsSingleSenderResult;
import com.github.qcloudsms.httpclient.HTTPException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSendException;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.Random;

@Component
public class SendSmsUtil {

    @Autowired
    JavaMailSenderImpl mailSender;

    public String sendSms(String phone, String code) {
        // 生成随机数
        SmsSingleSenderResult result = null;
        try {
            // 模板需要的参数
            String[] params = {code, "5"};
            SmsSingleSender ssender = new SmsSingleSender(SmsConstant.APP_ID, SmsConstant.APP_KEY);
            // 单发短信
            result = ssender.sendWithParam("86", phone, SmsConstant.TEMPLATE_ID, params, SmsConstant.SIGN, "", "");
        } catch (JSONException e) {
            // json解析错误
            e.printStackTrace();
        } catch (IOException e) {
            // 网络IO错误
            e.printStackTrace();
        } catch (HTTPException e) {
            e.printStackTrace();
        }

        if (!"OK".equals(result.errMsg)) {
            return "验证码发送失败";
        }
        return result.errMsg;
    }

    public String sendMail(String desAddress, String code) throws MessagingException {
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
        String msg = "OK";
        //标题
        helper.setSubject("calorie验证码为");
        //内容
        helper.setText("您好！您的验证码为：" + "<h2>" + code + "</h2>", true);
        //邮件接收者
        helper.setTo(desAddress);
        //邮件发送者，必须和配置文件里的一样，不然授权码匹配不上
        helper.setFrom("3275284157@qq.com");
        try {
            mailSender.send(mimeMessage);
        } catch (MailSendException e) {
            msg = "fail";
        }
        return msg;
    }

    public String sendMsgByPhone(String phone, String[] params, Integer templateId) {
        SmsSingleSenderResult result = null;
        try {
            // 模板需要的参
            SmsSingleSender ssender = new SmsSingleSender(SmsConstant.APP_ID, SmsConstant.APP_KEY);
            // 单发短信
            result = ssender.sendWithParam("86", phone, templateId, params, SmsConstant.SIGN, "", "");
        } catch (JSONException e) {
            // json解析错误
            e.printStackTrace();
        } catch (IOException e) {
            // 网络IO错误
            e.printStackTrace();
        } catch (HTTPException e) {
            e.printStackTrace();
        }

        if (!"OK".equals(result.errMsg)) {
            return "验证码发送失败";
        }
        return result.errMsg;
    }

    public String sendMsgByMail(String desAddress, String subject, String content) throws MessagingException {
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
        String msg = "OK";
        //标题

        helper.setSubject(subject);
        //内容
        helper.setText(content, true);


        //邮件接收者
        helper.setTo(desAddress);
        //邮件发送者，必须和配置文件里的一样，不然授权码匹配不上
        helper.setFrom("3275284157@qq.com");
        try {
            mailSender.send(mimeMessage);
        } catch (MailSendException e) {
            msg = "fail";
        }
        return msg;
    }

    /**
     * 验证码长度（通过更改i的最大值）
     * 获取6位随机数
     *
     * @return
     */
    public String createRandom() {
        Random random = new Random();
        String result = "";
        for (int i = 0; i < 6; i++) {
            result += random.nextInt(10);
        }
        return result;
    }
}
