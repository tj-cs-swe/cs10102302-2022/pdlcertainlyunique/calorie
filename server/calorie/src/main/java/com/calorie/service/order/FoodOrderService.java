package com.calorie.service.order;

import cn.hutool.core.date.DateTime;
import com.calorie.common.dto.UserCommentsDto;
import com.calorie.mapper.account.AccountInfoMapper;
import com.calorie.mapper.order.FoodOrderMapper;
import com.calorie.model.FoodOrder;
import com.calorie.model.User;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Service
public class FoodOrderService {
    @Autowired
    FoodOrderMapper orderMapper;

    public List<FoodOrder> selectAllFoodOrderByShopId(Integer shopId) {
        List<FoodOrder> foodOrderList = orderMapper.selectAllFoodOrderByShopId(shopId);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        for (FoodOrder foodOrder : foodOrderList) {
            foodOrder.setFoodOrderCreatedTimeFormatedStr(format.format(foodOrder.getFoodOrderCreatedTime()));
        }
        return foodOrderList;
    }

    public List<FoodOrder> selectFoodOrderByShopIdFoodOrderStatus(Integer shopId, Integer foodOrderStatus) {
        List<FoodOrder> foodOrderList = orderMapper.selectFoodOrderByShopIdFoodOrderStatus(shopId, foodOrderStatus);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        for (FoodOrder foodOrder : foodOrderList) {
            foodOrder.setFoodOrderCreatedTimeFormatedStr(format.format(foodOrder.getFoodOrderCreatedTime()));
        }
        return foodOrderList;
    }

    public FoodOrder selectFoodOrderByFoodOrderId(Integer foodOrderId) {
        FoodOrder foodOrder = orderMapper.selectFoodOrderByFoodOrderId(foodOrderId);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        foodOrder.setFoodOrderCreatedTimeFormatedStr(format.format(foodOrder.getFoodOrderCreatedTime()));
        return foodOrder;
    }

    public int updateFoodOrderStatus(Integer foodOrderId) {
        return orderMapper.updateFoodOrderStatus(foodOrderId);

    }


    @Autowired
    FoodOrderMapper foodOrderMapper;
    @Autowired
    AccountInfoMapper accountInfoMapper;

    public List<FoodOrder> queryFoodOrderByUserId(Integer userId) {
        List<FoodOrder> foodOrderList = foodOrderMapper.queryFoodOrderByUserId(userId);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        for (FoodOrder foodOrder : foodOrderList) {
            foodOrder.setFoodOrderCreatedTimeFormatedStr(format.format(foodOrder.getFoodOrderCreatedTime()));
        }
        return foodOrderList;
    }

    public List<FoodOrder> queryFoodOrderByUserIdFoodOrderStatus(Integer userId, Integer foodOrderStatus) {
        List<FoodOrder> foodOrderList = foodOrderMapper.queryFoodOrderByUserIdFoodOrderStatus(userId, foodOrderStatus);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        for (FoodOrder foodOrder : foodOrderList) {
            foodOrder.setFoodOrderCreatedTimeFormatedStr(format.format(foodOrder.getFoodOrderCreatedTime()));
        }
        return foodOrderList;
    }

    public List<Float> addNewFoodOrder(FoodOrder foodOrder) {
        float totalCalorie = 0.0f;
        float totalPrice = 0.0f;
        for (List<Integer> i : foodOrder.getFoodIdAndNum()) {
            totalCalorie += foodOrderMapper.getFoodCalorie(i.get(0)) * i.get(1) * 1.0f;
            totalPrice += foodOrderMapper.getFoodPrice(i.get(0)) * i.get(1) * 1.0f;
        }
        foodOrder.setFoodOrderCalorie(totalCalorie);
        foodOrder.setFoodOrderPrice(totalPrice);
        foodOrder.setFoodOrderCreatedTime(new DateTime());
        foodOrder.setFoodOrderStatus(0);
        foodOrder.setFoodOrderRiderSend(null);
        foodOrderMapper.insertFoodOrder(foodOrder);
        Integer foodOrderId = foodOrderMapper.getCurFoodOrderId();
        for (List<Integer> i : foodOrder.getFoodIdAndNum()) {
            foodOrderMapper.insertFoodFoodOrder(i.get(0), i.get(1), foodOrderId);
        }
        List<Float> res = new ArrayList<>();
        res.add(totalCalorie);
        res.add(totalPrice);
        return res;
    }

    public String queryCommentByFoodOrderId(Integer foodOrderId) {
        return foodOrderMapper.queryCommentByFoodOrderId(foodOrderId);
    }

    public List<UserCommentsDto> queryCommentsByShopId(Integer shopId) {
        return foodOrderMapper.queryCommentsByShopId(shopId);
    }

    public void confirmFoodOrder(Integer foodOrderId, Integer nextStatus) {
        foodOrderMapper.confirmFoodOrder(foodOrderId, nextStatus);
    }

    public FoodOrder queryFoodOrderByFoodOrderId(Integer foodOrderId) {
        FoodOrder foodOrder = foodOrderMapper.queryFoodOrderByFoodOrderId(foodOrderId);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        foodOrder.setFoodOrderCreatedTimeFormatedStr(format.format(foodOrder.getFoodOrderCreatedTime()));
        return foodOrder;
    }

    public void updateComment(Integer foodOrderId, String comment) {
        foodOrderMapper.updateComment(foodOrderId, comment);
    }

    public void updateFoodOrderRiderSend(Integer foodOrderId, Integer sendOrder) {
        foodOrderMapper.updateFoodOrderRiderSend(foodOrderId, sendOrder);
    }

    public Integer assignFoodOrderToRider(Integer foodOrderId, Integer riderUserId) {
        Integer curRiderUserId = foodOrderMapper.queryFoodOrderRider(foodOrderId);
        if (curRiderUserId == null) {
            if (riderUserId == -1) {
                List<Integer> ridersUserIds = foodOrderMapper.getAllRidersUserId();
                if (ridersUserIds.size() == 0)
                    return -1;
                Random random = new Random();
                int num = random.nextInt(ridersUserIds.size());
                foodOrderMapper.insertFoodOrderRider(foodOrderId, ridersUserIds.get(num));
                return ridersUserIds.get(num);
            } else {
                User usr = accountInfoMapper.selectByUserId(riderUserId);
                if (usr == null || usr.getUserIsRider() == 0)
                    return -2;
                foodOrderMapper.insertFoodOrderRider(foodOrderId, riderUserId);
            }
        } else {
            if (riderUserId != -1) {
                User usr = accountInfoMapper.selectByUserId(riderUserId);
                if (usr == null || usr.getUserIsRider() == 0)
                    return -2;
                foodOrderMapper.updateFoodOrderRider(foodOrderId, riderUserId);
            }
        }
        return riderUserId;
    }

    public Integer riderGetFoodOrder(Integer foodOrderId, Integer nextStatus)
    {
        FoodOrder foodOrder = foodOrderMapper.queryFoodOrderByFoodOrderId(foodOrderId);
        if (foodOrder.getFoodOrderStatus() != 6)
        {
            return -1;
        }
        foodOrderMapper.confirmFoodOrder(foodOrderId, nextStatus);
        return 0;
    }

    public List<FoodOrder> getAllFoodOrdersAssignedToSomeRider(Integer riderUserId){
        List<Integer> foodOrderIdList = foodOrderMapper.queryFoodOrderAssignedToRider(riderUserId);
        List<FoodOrder> foodOrderList = new ArrayList<>();
        for (Integer i : foodOrderIdList)
        {
            FoodOrder foodOrder = foodOrderMapper.queryDetailedFoodOrderByFoodOrderId(i);
            foodOrderList.add(foodOrder);
        }
        return foodOrderList;
    }
}

