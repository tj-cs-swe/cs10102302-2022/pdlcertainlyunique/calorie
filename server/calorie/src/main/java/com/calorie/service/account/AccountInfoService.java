package com.calorie.service.account;

import cn.hutool.crypto.SecureUtil;
import com.calorie.mapper.account.AccountInfoMapper;
import com.calorie.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountInfoService {
    @Autowired
    AccountInfoMapper accountInfoMapper;

    public User selectByUserId(Integer userId) {
        return accountInfoMapper.selectByUserId(userId);
    }

    public Integer updateUserNickname(String userNickname, Integer userId) {
        return accountInfoMapper.updateUserNickname(userNickname, userId);
    }

    public Integer updateUserName(String userName, Integer userId) {
        return accountInfoMapper.updateUserName(userName, userId);
    }

    public Integer updateUserPassword(String userPassword, Integer userId) {
        String md5pwd = SecureUtil.md5(userPassword);
        return accountInfoMapper.updateUserPassword(md5pwd, userId);
    }

    // 以下为用户偏好相关
    public void addUserFoodTypePrefer(String userName, String prefer) {
        accountInfoMapper.addUserFoodTypePrefer(userName, prefer);
    }

    public void deleteUserFoodTypePrefer(String userName, String prefer) {
        accountInfoMapper.deleteUserFoodTypePrefer(userName, prefer);
    }

    public List<String> selectUserFoodTypePreferByUserName(String userName) {
        return accountInfoMapper.selectUserFoodTypePreferByUserName(userName);
    }

    public void addUserServiceTypePrefer(String userName, String prefer) {
        accountInfoMapper.addUserServiceTypePrefer(userName, prefer);
    }

    public void deleteUserServiceTypePrefer(String userName, String prefer) {
        accountInfoMapper.deleteUserServiceTypePrefer(userName, prefer);
    }

    public List<String> selectUserServiceTypePreferByUsernName(String userName) {
        return accountInfoMapper.selectUserServiceTypePreferByUsernName(userName);
    }

    public void updateWorkoutQuantityPreferByUserName(String userName, Integer quantity) {
        accountInfoMapper.updateWorkoutQuantityPreferByUserName(userName, quantity);
    }

    public void updateUserHealthInfo(User user) {
        accountInfoMapper.updateUserHealthInfo(user);
    }

    public void updateChangeNameRole(String oldname, String newname) {
        accountInfoMapper.updateChangeNameRole(oldname, newname);
    }
}
