package com.calorie.service.account;

import com.calorie.common.dto.MerchantRecognizeDto;
import com.calorie.common.dto.RiderRecognizeDto;
import com.calorie.mapper.account.AuthenticationMapper;
import com.calorie.model.User;
import com.calorie.utils.SendSmsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.util.List;

@Service
public class AuthenticationService {
    @Autowired
    AuthenticationMapper authenticationMapper;

    @Autowired
    SendSmsUtil sendSmsUtil;

    // 更新骑手认证
    public int updateRiderAuthInfo(int userIsRider, RiderRecognizeDto riderDto) {
        return authenticationMapper.updateRiderAuthInfo(userIsRider, riderDto);
    }

    // 更新审核
    public int updateUserIsRider(int userIsRider, String userName) {
        return authenticationMapper.updateUserIsRider(userIsRider, userName);
    }

    // 更新食品商家认证
    public int updateFoodMerchantAuthInfo(int userIsFoodMerchant, MerchantRecognizeDto merchantDto) {
        return authenticationMapper.updateFoodMerchantAuthInfo(userIsFoodMerchant, merchantDto);
    }

    // 更新审核
    public int updateUserIsFoodMerchant(int userIsFoodMerchant, String userName) {
        return authenticationMapper.updateUserIsFoodMerchant(userIsFoodMerchant, userName);
    }

    // 更新健身商家认证
    public int updateGymMerchantAuthInfo(int userIsGymMerchant, MerchantRecognizeDto merchantDto) {
        return authenticationMapper.updateGymMerchantAuthInfo(userIsGymMerchant, merchantDto);
    }

    // 更新审核
    public int updateUserIsGymMerchant(int userIsGymMerchant, String userName) {
        return authenticationMapper.updateUserIsGymMerchant(userIsGymMerchant, userName);
    }


    // 获取所有骑手认证申请
    public List<User> selectRiderAuthRequests() {
        return authenticationMapper.selectRiderAuthRequests();
    }

    // 获取所有食品商家认证申请
    public List<User> selectFoodMerchantAuthRequests() {
        return authenticationMapper.selectFoodMerchantAuthRequests();
    }

    // 获取所有健身商家认证申请
    public List<User> selectGymMerchantAuthRequests() {
        return authenticationMapper.selectGymMerchantAuthRequests();
    }

    public void addUserRole(String userName, String userRole) {
        authenticationMapper.addUserRole(userName, userRole);
    }

    public void deleteUserRole(String userName, String userRole) {
        authenticationMapper.deleteUserRole(userName, userRole);
    }

    public String sendAcceptAuthMsg(User user, String role) throws MessagingException {
        String result = "";
        if (!user.getUserEmail().equals("")) {
            String subject = "";
            String content = "";
            if (role.equals("rider")) {
                subject = "骑手审核通过";
                content = user.getUserName() + "您好！您的骑手身份认证已通过审核。";
            } else if (role.equals("foodMerchant")) {
                subject = "餐品商家审核通过";
                content = user.getUserName() + "您好！您的餐品商家身份认证已通过审核。";
            } else if (role.equals("gymMerchant")) {
                subject = "健身商家审核通过";
                content = user.getUserName() + "您好！您的健身商家身份认证已通过审核。";
            }

            result = sendSmsUtil.sendMsgByMail(user.getUserEmail(), subject, content);

        } else if (!user.getUserPhone().equals("")) {
            String[] params = new String[]{"-", "-", "-"};
            if (role.equals("rider")) {
                params = new String[]{user.getUserName(), "骑手审核", "-"};
            } else if (role.equals("foodMerchant")) {
                params = new String[]{user.getUserName(), "餐品商家审核", "-"};
            } else if (role.equals("gymMerchant")) {
                params = new String[]{user.getUserName(), "健身商家审核", "-"};
            }
            result = sendSmsUtil.sendMsgByPhone(user.getUserPhone(), params, 1432336);
        }
        return result;
    }

    public String sendRejectAuthMsg(User user, String role) throws MessagingException {
        String result = "";
        if (!user.getUserEmail().equals("")) {
            String subject = "";
            String content = "";
            if (role.equals("rider")) {
                subject = "骑手审核未通过";
                content = user.getUserName() + "您好！您的骑手身份认证未通过审核。";
            } else if (role.equals("foodMerchant")) {
                subject = "餐品商家审核未通过";
                content = user.getUserName() + "您好！您的餐品商家身份认证未通过审核。";
            } else if (role.equals("gymMerchant")) {
                subject = "健身商家审核未通过";
                content = user.getUserName() + "您好！您的健身商家身份认证未通过审核。";
            }

            result = sendSmsUtil.sendMsgByMail(user.getUserEmail(), subject, content);

        } else if (!user.getUserPhone().equals("")) {
            String[] params = new String[]{"-", "-", "-", "-"};
            if (role.equals("rider")) {
                params = new String[]{user.getUserName(), "骑手审核", "-", "-"};
            } else if (role.equals("foodMerchant")) {
                params = new String[]{user.getUserName(), "餐品商家审核", "-", "-"};
            } else if (role.equals("gymMerchant")) {
                params = new String[]{user.getUserName(), "健身商家审核", "-", "-"};
            }
            result = sendSmsUtil.sendMsgByPhone(user.getUserPhone(), params, 1432335);
        }
        return result;
    }
}
