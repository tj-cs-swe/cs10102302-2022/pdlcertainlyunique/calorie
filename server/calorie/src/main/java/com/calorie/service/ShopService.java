package com.calorie.service;

import com.calorie.mapper.ShopMapper;
import com.calorie.model.Shop;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShopService {
    @Autowired
    ShopMapper shopMapper;

    public int insertNewShop(Shop shop) {
        return shopMapper.insertNewShop(shop);
    }

    public int insertNewShopUser(Integer shopId, Integer userId) {
        return shopMapper.insertNewShopUser(shopId, userId);
    }

    public Shop selectShopByShopIdShopType(Integer shopId, String shopType) {
        return shopMapper.selectShopByShopIdShopType(shopId, shopType);
    }

    public Shop selectShopByShopIdShopTypeUserId(Integer shopId, String shopType, Integer userId) {
        return shopMapper.selectShopByShopIdShopTypeUserId(shopId, shopType, userId);
    }

    public List<Integer> selectAllFoodShopId() {
        return shopMapper.selectAllFoodShopId();
    }

    public List<Integer> selectAllServiceShopId() {
        return shopMapper.selectAllServiceShopId();
    }
}
