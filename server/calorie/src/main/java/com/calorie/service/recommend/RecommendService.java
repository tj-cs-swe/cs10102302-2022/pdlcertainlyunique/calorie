package com.calorie.service.recommend;

import com.calorie.model.Food;
import com.calorie.model.FoodOrder;
import com.calorie.model.ServiceOrder;
import com.calorie.model.User;
import com.calorie.service.ShopService;
import com.calorie.service.account.AccountInfoService;
import com.calorie.service.account.UserService;
import com.calorie.service.food.FoodService;
import com.calorie.service.healthcalc.HealthCalcService;
import com.calorie.service.order.FoodOrderService;
import com.calorie.service.order.ServiceOrderService;
import com.calorie.service.workout.WorkoutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class RecommendService {
    @Autowired
    UserService userService;
    @Autowired
    AccountInfoService accountInfoService;

    @Autowired
    HealthCalcService healthCalcService;
    // food part
    @Autowired
    FoodService foodService;
    @Autowired
    ShopService shopService;
    @Autowired
    FoodOrderService foodOrderService;
    @Autowired
    ServiceOrderService serviceOrderService;

    private final double EPS = 1e-8;

    public Map<Food, Integer> foodRecommendation(User user, int shopId) { // 餐品推荐
        double sum = 0;

        sum -= calcTodayFoodOverflowCalorie(user);

        // System.out.println(calcTodayFoodOverflowCalorie(user));

        double currentCalorie = healthCalcService.calcCalorieIntakeExpectationByTime(user, new Date());
        sum += currentCalorie;

        // 加上运动卡路里消耗
        sum += calcTodayWorkoutCalorie(user);
        // System.out.println(calcTodayWorkoutCalorie(user));
        return getFoods(user, shopId, sum - currentCalorie * 0.10, sum + currentCalorie * 0.10);
    }

    // 当日食品订单 (已完成）
    private List<FoodOrder> todayFoodOrders(User user) { // 当日
        List<FoodOrder> retFoodOrders = new ArrayList<>();
        List<FoodOrder> foodOrders = foodOrderService.queryFoodOrderByUserId(user.getUserId());

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String curDateStr = df.format(new Date());

        for (FoodOrder foodOrder : foodOrders) {
            String foodOCT = df.format(foodOrder.getFoodOrderCreatedTime());
            // 找到当日已配送订单
            if (foodOCT.equals(curDateStr) && (foodOrder.getFoodOrderStatus() == 3 || foodOrder.getFoodOrderStatus() == 4))
                retFoodOrders.add(foodOrder);
        }

        return retFoodOrders;
    }

    private double calcTodayFoodOverflowCalorie(User user) {
        double overflow = 0;
        double daily = healthCalcService.calcDailyCalorie(user);

        double breakfast = daily * 0.25;
        double lunch = daily * 0.4;
        double dinner = daily * 0.35;

        boolean containBreakfast = false;
        boolean containLunch = false;
        boolean containDinner = false;

        SimpleDateFormat df = new SimpleDateFormat("HH");
        for (FoodOrder foodOrder : todayFoodOrders(user)) {
            Date date = foodOrder.getFoodOrderCreatedTime();
            String str = df.format(date);
            int hour = Integer.parseInt(str);

            // 判定订单对应早中晚餐
            if (hour <= 10) { // breakfast
                containBreakfast = true;
                breakfast -= foodOrder.getFoodOrderCalorie();
            } else if (11 <= hour && hour <= 15) { // lunch
                containLunch = true;
                lunch -= foodOrder.getFoodOrderCalorie();
            } else { // dinner
                containDinner = true;
                dinner -= foodOrder.getFoodOrderCalorie();
            }
        }

        if (containBreakfast) overflow -= breakfast;
        if (containLunch) overflow -= lunch;
        if (containDinner) overflow -= dinner;
        return overflow;
    }

    private double calcTodayWorkoutCalorie(User user) {
        double sum = 0;

        double daily = healthCalcService.calcDailyCalorie(user);

        double breakfast = daily * 0.25;
        double lunch = daily * 0.4;
        double dinner = daily * 0.35;

        boolean containBreakfast = false;
        boolean containLunch = false;
        boolean containDinner = false;

        SimpleDateFormat df = new SimpleDateFormat("HH");
        for (ServiceOrder serviceOrder : todayServiceOrder(user)) {
            Date date = serviceOrder.getServiceOrderCreatedTime();
            String str = df.format(date);
            int hour = Integer.parseInt(str);

            if (hour <= 11) { // breakfast
                containBreakfast = true;
            } else if (12 <= hour && hour <= 16) { // lunch
                containLunch = true;
            } else { // dinner
                containDinner = true;
            }

            sum += serviceOrder.getServiceOrderTotalCalorie();
        }

        double coef = healthCalcService.calcCoef(user);
        int hour = Integer.parseInt(df.format(new Date()));

        if (!containBreakfast && hour >= 10) sum -= breakfast * (coef - 1) / coef;
        if (!containLunch && hour >= 15) sum -= lunch * (coef - 1) / coef;
        if (!containDinner && hour >= 19) sum -= dinner * (coef - 1) / coef;
        return sum;
    }

    // 当日健身订单（已完成）
    private List<ServiceOrder> todayServiceOrder(User user) {
        List<ServiceOrder> retServiceOrders = new ArrayList<>();
        List<ServiceOrder> serviceOrders = serviceOrderService.queryServiceOrderByUserId(user.getUserId());

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String curDateStr = df.format(new Date());

        for (ServiceOrder serviceOrder : serviceOrders) {
            String serviceOCT = df.format(serviceOrder.getServiceOrderCreatedTime());
            // 找到当日已进行的健身服务
            if (serviceOCT.equals(curDateStr) && serviceOrder.getServiceOrderStatus() == 4)
                retServiceOrders.add(serviceOrder);
        }

        return retServiceOrders;
    }

    private Map<Food, Integer> getFoods(User user, int shopId, double l, double r) { // 获取推荐食物
        List<Food> foods = foodService.selectFoodByShopIdStatus(shopId); // 获取商家餐品

        Map<String, Integer> fav = new HashMap<>();
        Map<Integer, Integer> recent = new HashMap<>();

        for (Food food : foods) {
            String foodType = food.getFoodCategory();
            fav.put(foodType, fav.getOrDefault(foodType, 0) + 1);
        }

        int maxfav = 1;
        //  获取用户所有订单
        List<FoodOrder> foodOrders = foodOrderService.queryFoodOrderByUserId(user.getUserId());
        for (FoodOrder foodOrder : foodOrders) {
            for (Food food : foodOrder.getFoodList()) {
                String foodCategory = food.getFoodCategory();

                fav.put(foodCategory, fav.getOrDefault(foodCategory, 0) + 1);

                maxfav = Math.max(maxfav, fav.getOrDefault(foodCategory, 0));

                recent.put(food.getFoodId(), recent.getOrDefault(food.getFoodId(), 0) + 2);
            }
        }

        // 还要考虑用户餐品类型偏好
        List<String> userFoodPefers = accountInfoService.selectUserFoodTypePreferByUserName(user.getUserName());
        for (String userFoodPefer : userFoodPefers) {
            fav.put(userFoodPefer, fav.getOrDefault(userFoodPefer, 0) + maxfav * 2);
        }

        // 按偏好、最近购买、卡路里排序
        foods.sort(new Comparator<Food>() {
            @Override
            public int compare(Food o1, Food o2) {
                int value1 = fav.getOrDefault(o1.getFoodCategory(), 0) - recent.getOrDefault(o1.getFoodId(), 0);
                int value2 = fav.getOrDefault(o2.getFoodCategory(), 0) - recent.getOrDefault(o2.getFoodId(), 0);
                if (value1 == value2) {
                    return -o1.getFoodCalorie().compareTo(o2.getFoodCalorie());
                } else if (value1 < value2) {
                    return 1;
                } else {
                    return -1;
                }
            }
        });

        Map<Food, Integer> recommendedOrder = new HashMap<>(); // 餐品、数量
        Random rand = new Random(); // 随机数生成器
        double sum = 0;
        int count = 0;

        while (!(l < sum + EPS && sum < r + EPS)) {
            count++;
            double pre = sum;
            for (Food food : foods) {
                if (Math.abs(food.getFoodCalorie()) < EPS) continue; // 0 卡路里，不推荐
                if (foods.size() > 1 && rand.nextInt() % 6 == 0) continue;
                if (sum + food.getFoodCalorie() < r + EPS) {
                    recommendedOrder.put(food, recommendedOrder.getOrDefault(food, 0) + 1);
                    sum += food.getFoodCalorie();
                }
            }
            if (Math.abs(pre - sum) < EPS || sum > r || count > r) break;
        }
        return recommendedOrder;
    }

    // sport part
    @Autowired
    WorkoutService workoutService;

    public Map<com.calorie.model.Service, Integer> workoutRecommendation(User user, int shopId) {
        double sum = 0;
        final double LIMIT = 40;

        sum += calcTodayFoodOverflowCalorie(user);

        // 减去已进行运动卡路里消耗
        sum -= calcTodayWorkoutCalorie(user);

        if (sum > LIMIT - EPS) return getWorkout(user, shopId, sum);
        else return new HashMap<>();
    }

    // 健身运动推荐 根据用户健身类型偏好 + 随机
    private Map<com.calorie.model.Service, Integer> getWorkout(User user, int shopId, double sumCalorie) {
        List<com.calorie.model.Service> workouts = workoutService.selectServiceByShopId(shopId);

        Map<com.calorie.model.Service, Integer> recommendedWorkouts = new HashMap<>(); // 健身运动、运动时间

        Map<String, Integer> fav = new HashMap<>();
        List<String> userPrefer = userService.selectServicePreferByUserName(user.getUserName());
        for (String s : userPrefer) fav.put(s, 1);

        // 偏好运动靠前
        workouts.sort(new Comparator<com.calorie.model.Service>() {
            @Override
            public int compare(com.calorie.model.Service o1, com.calorie.model.Service o2) {
                boolean v1 = fav.containsKey(o1.getServiceCategory());
                boolean v2 = fav.containsKey(o2.getServiceCategory());
                if (!v1 && v2) return 1;
                else if (v1 == v2) return 0;
                else return -1;
            }
        });

        Random rand = new Random(); // 随机数生成器
        final int limit = 8;
        for (com.calorie.model.Service workout : workouts) { // 先随机推荐
            if (recommendedWorkouts.size() >= limit) break;
            if (rand.nextInt() % Math.max(1, (workouts.size() / 3)) == 0) {
                // 不推荐运动时间过长的运动
                if (Math.round(Math.ceil(sumCalorie / workout.getServiceCalorie())) > 90) continue;
                recommendedWorkouts.put(workout, (int) Math.round(Math.ceil(sumCalorie / workout.getServiceCalorie())));
            }
        }
        for (com.calorie.model.Service workout : workouts) { // 没满 limit 则再顺序推荐
            if (recommendedWorkouts.size() >= limit) break;
            if (!recommendedWorkouts.containsKey(workout)) {
                // 不推荐运动时间过长的运动
                if (Math.round(Math.ceil(sumCalorie / workout.getServiceCalorie())) > 60) continue;
                recommendedWorkouts.put(workout, (int) Math.round(Math.ceil(sumCalorie / workout.getServiceCalorie())));
            }
        }
        return recommendedWorkouts;
    }
}
