package com.calorie.service.healthcalc;

import com.calorie.model.User;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class HealthCalcService {
    // 计算 BMI
    public double calcUserBMI(User user) {
        return user.getUserWeight() / (user.getUserHeight() * user.getUserHeight()) * 10000;
    }

    // 计算 REE(静态能量消耗值)
    public double calcUserREE(User user) {
        if (user.getUserSex().equals("femal")) { // femal
            return 6.25 * user.getUserHeight() + 10.0 * user.getUserWeight() - 5.0 * user.getUserAge() - 161;
        } else { // male
            return 6.25 * user.getUserHeight() + 10.0 * user.getUserWeight() - 5.0 * user.getUserAge() + 5;
        }
    }

    public double calcCoef(User user) {
        double coef = 1.2; // 卧床
        switch (user.getUserWorkoutQuantityPrefer()) {
            case 1:
                coef = 1.3; // 轻活动（多坐、缓步）
                break;
            case 2:
                coef = (1.5 + 1.75) / 2; // 一般活动度
                break;
            case 3:
                coef = 2.0; // 活动量大
                break;
            default:
                break;
        }
        return coef;
    }

    // 每日所需热量
    public double calcDailyCalorie(User user) {
        return calcCoef(user) * this.calcUserREE(user);
    }

    // 估算就餐时间段所需摄入的热量
    public double calcCalorieIntakeExpectationByTime(User user, Date date) {
        SimpleDateFormat df = new SimpleDateFormat("HH");
        String str = df.format(date);
        int hour = Integer.parseInt(str);
        if (hour <= 10) { // breakfast
            return this.calcDailyCalorie(user) * 0.25;
        } else if (11 <= hour && hour <= 15) { // lunch
            return this.calcDailyCalorie(user) * 0.4;
        } else { // dinner
            return this.calcDailyCalorie(user) * 0.35;
        }
    }
};
