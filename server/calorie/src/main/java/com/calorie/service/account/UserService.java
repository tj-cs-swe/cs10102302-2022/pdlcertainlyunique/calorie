package com.calorie.service.account;

import cn.hutool.core.map.MapUtil;
import cn.hutool.crypto.SecureUtil;
import com.calorie.common.dto.RegisterDto;
import com.calorie.common.lang.Result;
import com.calorie.common.message.SmsConstant;
import com.calorie.mapper.account.UserMapper;
import com.calorie.model.User;
import com.calorie.util.RedisUtil;
import com.calorie.utils.SendSmsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.mail.MessagingException;
import java.util.List;

@Service
public class UserService {
    @Autowired
    UserMapper userMapper;

    @Autowired
    SendSmsUtil sendSmsUtil;

    @Autowired
    RedisUtil redisUtil;

    public User selectUserByShopId(Integer shopId) {
        return userMapper.selectUserByShopId(shopId);
    }

    public User selectByUserName(String userName) {
        return userMapper.selectByUserName(userName);
    }

    public List<String> selectRolesByUserName(String userName) {
        return userMapper.selectRolesByUserName(userName);
    }

    public List<String> selectServicePreferByUserName(String userName) {
        return userMapper.selectServicePreferByUserName(userName);
    }

    public List<String> selectFoodPreferByUserName(String userName) {
        return userMapper.selectFoodPreferByUserName(userName);
    }

    //发送手机验证码
    public String sendPhoneCode(String phone) {
        String code = sendSmsUtil.createRandom();
        String result = sendSmsUtil.sendSms(phone, code);

        if ("OK".equals(result)) {
            redisUtil.set(phone, code, SmsConstant.EFFECTIVE_Time);
        }
        return result;
    }

    //发送邮箱验证码
    public String sendEmailCode(String email) throws MessagingException {
        String code = sendSmsUtil.createRandom();
        String result = sendSmsUtil.sendMail(email, code);

//        String result = "OK";
        if ("OK".equals(result)) {
            redisUtil.set(email, code, SmsConstant.EFFECTIVE_Time);
        }

        return result;
    }


    public Result phoneRegisterService(RegisterDto registerDto) {
        User user = userMapper.selectByUserPhone(registerDto.getUserPhone());
        Assert.isNull(user, "手机号已被占用");

        String codeInfo = "未发送验证码";

        //发送验证码
        if (registerDto.getStatus().equals("1")) {
            codeInfo = sendPhoneCode(registerDto.getUserPhone());
        }

        //校验验证码
        else if (registerDto.getStatus().equals("2")) {
            String phone = registerDto.getUserPhone();
            String code = redisUtil.get(phone);
            Assert.notNull(code, "验证码已失效");

            if (!code.equals(registerDto.getVerifyCode())) {
                return Result.fail("验证码错误");
            }
            codeInfo = "验证成功";

            //清除验证码
            redisUtil.del(phone);

            registerDto.setUserPassword(SecureUtil.md5(registerDto.getUserPassword()));

            int insertNum = userMapper.insertUser(registerDto);

            int insertNum2 = userMapper.insertUserRole(registerDto.getUserName());

            if (insertNum != 1 || insertNum2 != 1) {
                return Result.fail("系统异常");
            }
        }

        return Result.succ(MapUtil.builder()
                .put("username", registerDto.getUserName())
                .put("password", registerDto.getUserPassword())
                .put("验证码", codeInfo)
                .map()
        );

    }

    public Result emailRegisterService(RegisterDto registerDto) throws MessagingException {
        User user = userMapper.selectByUserEmail(registerDto.getUserEmail());
        Assert.isNull(user, "邮箱已被占用");

        String codeInfo = "未发送验证码";

        //发送验证码
        if (registerDto.getStatus().equals("1")) {
            codeInfo = sendEmailCode(registerDto.getUserEmail());
            if (!codeInfo.equals("OK")) {
                return Result.fail("验证码发送失败");
            }
        }

        //校验验证码
        else if (registerDto.getStatus().equals("2")) {
            String email = registerDto.getUserEmail();
            String code = redisUtil.get(email);
            Assert.notNull(code, "验证码已失效");

            if (!code.equals(registerDto.getVerifyCode())) {
                return Result.fail("验证码错误");
            }
            codeInfo = "验证成功";

            //清除验证码
            redisUtil.del(email);

            registerDto.setUserPassword(SecureUtil.md5(registerDto.getUserPassword()));

            int insertNum = userMapper.insertUser(registerDto);

            int insertNum2 = userMapper.insertUserRole(registerDto.getUserName());

            if (insertNum != 1 || insertNum2 != 1) {
                return Result.fail("系统异常");
            }
        }

        return Result.succ(MapUtil.builder()
                .put("username", registerDto.getUserName())
                .put("password", registerDto.getUserPassword())
                .put("验证码", codeInfo)
                .map()
        );

    }
}
