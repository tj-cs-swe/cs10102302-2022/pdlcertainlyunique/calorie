package com.calorie.service.food;


import com.calorie.mapper.ShopMapper;
import com.calorie.mapper.food.FoodCensorMapper;
import com.calorie.mapper.food.FoodMapper;
import com.calorie.model.Food;
import com.calorie.model.FoodCensor;
import com.calorie.model.Shop;
import com.calorie.model.User;
import com.calorie.utils.SendSmsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.util.Date;
import java.util.List;

@Service
public class FoodService {

    @Autowired
    FoodMapper foodMapper;

    @Autowired
    ShopMapper shopMapper;

    @Autowired
    SendSmsUtil sendSmsUtil;

    @Autowired
    FoodCensorMapper foodCensorMapper;

    public List<Food> selectAllFood() {
        return foodMapper.selectAllFood();
    }

    public List<Shop> selectShopByShopType(String shopType) {
        return shopMapper.selectShopByShopType(shopType);
    }

    public List<Food> selectFoodByShopIdStatus(Integer shopId) {
        return foodMapper.selectFoodByShopIdStatus(shopId);
    }

    public Food selectFoodByFoodIdShopId(Integer foodId, Integer shopId) {
        return foodMapper.selectFoodByFoodIdShopId(foodId, shopId);
    }

    public List<Shop> selectShopByShopTypeUserId(String shopType, Integer userId) {
        return shopMapper.selectShopByShopTypeUserId(shopType, userId);
    }

    public List<Food> selectAllFoodByShopId(Integer shopId) {
        return foodMapper.selectAllFoodByShopId(shopId);
    }

    public Shop selectShopFoodsByShopId(Integer shopId, String shopType) {
        return shopMapper.selectShopFoodsByShopId(shopId, shopType);
    }

    public Shop selectShopFoodsByShopIdFoodStatus(Integer shopId, String shopType) {
        return shopMapper.selectShopFoodsByShopIdFoodStatus(shopId, shopType);
    }


    public int insertNewFoodForCensor(FoodCensor foodCensor, Integer shopId) {
        foodCensor.setFoodCensorCreateDate(new Date());
        foodCensor.setFoodCensorStatus(0);
        foodCensor.setFoodShopId(shopId);
        return foodCensorMapper.insertNewFoodForCensor(foodCensor);
    }

//    public int updateFood(FoodDto foodDto) {
//        return foodMapper.updateFoodByFoodId(foodDto);
//    }

    public int updateFoodForCensor(FoodCensor foodCensor, Integer shopId) {
        foodCensor.setFoodCensorCreateDate(new Date());
        foodCensor.setFoodCensorStatus(0);
        foodCensor.setFoodShopId(shopId);
        return foodCensorMapper.insertNewFoodForCensor(foodCensor);
    }


    public int updateFoodTakeStatus(Integer foodTakeStatus, Integer foodId) {
        return foodMapper.updateFoodTakeStatus(foodTakeStatus, foodId);
    }

    public int insertNewFood(Integer foodCensorId) {
        FoodCensor foodCensor = foodCensorMapper.selectFoodCensorByFoodId(foodCensorId);
        if (foodCensor == null) {
            return 0;
        }
        foodCensor.setFoodTakeStatus(1);
        if (foodCensor.getFoodCensorType() == 0) {
            return foodMapper.insertNewFood(foodCensor);
        } else {
            foodCensor.setFoodId(foodCensor.getFoodCensorType());
            return foodMapper.updateFoodByFoodId(foodCensor);
        }
    }

    public int deleteFoodCensorByFoodCensorId(Integer foodCensorId) {
        return foodCensorMapper.deleteFoodCensorByFoodCensorId(foodCensorId);
    }

    public int updateRejectFoodMsg(FoodCensor foodCensor) {
        return foodCensorMapper.updateRejectFoodMsg(foodCensor);
    }

    public List<FoodCensor> selectAllFoodCensor() {
        return foodCensorMapper.selectAllFoodCensor();
    }

    public FoodCensor selectFoodCensorByFoodId(Integer foodCensorId) {
        return foodCensorMapper.selectFoodCensorByFoodId(foodCensorId);
    }

    public String sendAcceptFoodMsg(FoodCensor foodCensor, User user) throws MessagingException {
        String result = "";
        if (!user.getUserEmail().equals("")) {
            String subject;
            String content;
            if (foodCensor.getFoodCensorType() == 0) {
                subject = "添加餐品\"" + foodCensor.getFoodName() + "\"审核通过";
                content = user.getUserName() + "您好！您添加的\"" + foodCensor.getFoodName() + "\"餐品已通过审核。";
            } else {
                subject = "更新餐品\"" + foodCensor.getFoodName() + "\"审核通过";
                content = user.getUserName() + "您好！您更新的\"" + foodCensor.getFoodName() + "\"餐品已通过审核。";
            }

            result = sendSmsUtil.sendMsgByMail(user.getUserEmail(), subject, content);

        } else if (!user.getUserPhone().equals("")) {
            String[] params = new String[]{"-", "-", "-"};
            if (foodCensor.getFoodCensorType() == 0) {
                params = new String[]{user.getUserName(), "添加餐品审核", foodCensor.getFoodName()};
            } else {
                params = new String[]{user.getUserName(), "更新餐品审核", foodCensor.getFoodName()};
            }
            result = sendSmsUtil.sendMsgByPhone(user.getUserPhone(), params, 1432336);
        }
        return result;
    }

    public String sendRejectFoodMsg(FoodCensor foodCensor, User user) throws MessagingException {
        String result = "";
        String subject;
        String content;
        String msg = foodCensor.getFoodCensorMsg();
        if (foodCensor.getFoodCensorMsg().equals("")) {
            msg = "无";
        }
        if (!user.getUserEmail().equals("")) {

            if (foodCensor.getFoodCensorType() == 0) {
                subject = "添加餐品\"" + foodCensor.getFoodName() + "\"审核未通过";
                content = user.getUserName() + "您好！您添加的\"" + foodCensor.getFoodName() + "\"餐品未通过审核，原因是：" + msg;
            } else {
                subject = "更新餐品\"" + foodCensor.getFoodName() + "\"审核未通过";
                content = user.getUserName() + "您好！您更新的\"" + foodCensor.getFoodName() + "\"餐品未通过审核，原因是：" + msg;
            }

            result = sendSmsUtil.sendMsgByMail(user.getUserEmail(), subject, content);

        } else if (!user.getUserPhone().equals("")) {
            String[] params = new String[]{"-", "-", "-"};
            if (foodCensor.getFoodCensorType() == 0) {
                params = new String[]{user.getUserName(), "添加餐品审核", foodCensor.getFoodName(), msg};
            } else {
                params = new String[]{user.getUserName(), "更新餐品审核", foodCensor.getFoodName(), msg};
            }
            result = sendSmsUtil.sendMsgByPhone(user.getUserPhone(), params, 1432335);
        }
        return result;
    }
}
