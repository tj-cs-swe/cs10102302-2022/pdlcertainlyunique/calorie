package com.calorie.service.workout;

import com.calorie.mapper.ShopMapper;
import com.calorie.mapper.workout.ServiceCensorMapper;
import com.calorie.mapper.workout.ServiceMapper;
import com.calorie.model.ServiceCensor;
import com.calorie.model.Shop;
import com.calorie.model.User;
import com.calorie.utils.SendSmsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.util.Date;
import java.util.List;

@Service
public class WorkoutService {
    @Autowired
    ShopMapper shopMapper;

    @Autowired
    ServiceMapper serviceMapper;

    @Autowired
    SendSmsUtil sendSmsUtil;

    @Autowired
    ServiceCensorMapper serviceCensorMapper;

    public List<com.calorie.model.Service> selectAllService() {
        return serviceMapper.selectAllService();
    }

    public List<Shop> selectShopByShopType(String shopType) {
        return shopMapper.selectShopByShopType(shopType);
    }

    public List<com.calorie.model.Service> selectServiceByShopId(Integer shopId) {
        return serviceMapper.selectServiceByShopId(shopId);
    }

    public Shop selectShopServicesByShopId(Integer shopId, String shopType) {
        return shopMapper.selectShopServicesByShopId(shopId, shopType);
    }

    public com.calorie.model.Service selectServiceByServiceIdShopId(Integer serviceId, Integer shopId) {
        return serviceMapper.selectServiceByServiceIdShopId(serviceId, shopId);
    }

    public List<Shop> selectShopByShopTypeUserId(String shopType, Integer userId) {
        return shopMapper.selectShopByShopTypeUserId(shopType, userId);
    }


    public int insertNewService(Integer serviceCensorId) {
        ServiceCensor serviceCensor = serviceCensorMapper.selectServiceCensorByServiceId(serviceCensorId);
        if (serviceCensorId == null) {
            return 0;
        }
        if (serviceCensor.getServiceCensorType() == 0) {
            return serviceMapper.insertNewService(serviceCensor);
        } else {
            serviceCensor.setServiceId(serviceCensor.getServiceCensorType());
            return serviceMapper.updateServiceByServiceId(serviceCensor);
        }
    }

    public int insertNewServiceForCensor(ServiceCensor serviceCensor, Integer shopId) {
        serviceCensor.setServiceCensorCreateDate(new Date());
        serviceCensor.setServiceCensorStatus(0);
        serviceCensor.setServiceShopId(shopId);
        return serviceCensorMapper.insertNewServiceForCensor(serviceCensor);
    }

    public int updateServiceForCensor(ServiceCensor serviceCensor, Integer shopId) {
        serviceCensor.setServiceCensorCreateDate(new Date());
        serviceCensor.setServiceCensorStatus(0);
        serviceCensor.setServiceShopId(shopId);
        return serviceCensorMapper.insertNewServiceForCensor(serviceCensor);
    }

    public int deleteNewService(Integer serviceId) {
        return serviceMapper.deleteNewService(serviceId);
    }

    public int updateRejectServiceMsg(ServiceCensor serviceCensor) {
        return serviceCensorMapper.updateRejectServiceMsg(serviceCensor);
    }

    public List<ServiceCensor> selectAllServiceCensor() {
        return serviceCensorMapper.selectAllServiceCensor();
    }

    public ServiceCensor selectServiceCensorByServiceId(Integer serviceCensorId) {
        return serviceCensorMapper.selectServiceCensorByServiceId(serviceCensorId);
    }

    public String sendAcceptServiceMsg(ServiceCensor serviceCensor, User user) throws MessagingException {
        String result = "";
        if (!user.getUserEmail().equals("")) {
            String subject;
            String content;
            if (serviceCensor.getServiceCensorType() == 0) {
                subject = "添加服务\"" + serviceCensor.getServiceName() + "\"审核通过";
                content = user.getUserName() + "您好！您添加的\"" + serviceCensor.getServiceName() + "\"服务已通过审核。";
            } else {
                subject = "更新服务\"" + serviceCensor.getServiceName() + "\"审核通过";
                content = user.getUserName() + "您好！您更新的\"" + serviceCensor.getServiceName() + "\"服务已通过审核。";
            }

            result = sendSmsUtil.sendMsgByMail(user.getUserEmail(), subject, content);

        } else if (!user.getUserPhone().equals("")) {
            String[] params = new String[]{"-", "-", "-"};
            if (serviceCensor.getServiceCensorType() == 0) {
                params = new String[]{user.getUserName(), "添加服务审核", serviceCensor.getServiceName()};
            } else {
                params = new String[]{user.getUserName(), "更新服务审核", serviceCensor.getServiceName()};
            }
            result = sendSmsUtil.sendMsgByPhone(user.getUserPhone(), params, 1432336);
        }
        return result;
    }

    public String sendRejectServiceMsg(ServiceCensor serviceCensor, User user) throws MessagingException {
        String result = "";
        String subject;
        String content;
        String msg = serviceCensor.getServiceCensorMsg();
        if (serviceCensor.getServiceCensorMsg().equals("")) {
            msg = "无";
        }
        if (!user.getUserEmail().equals("")) {

            if (serviceCensor.getServiceCensorType() == 0) {
                subject = "添加服务\"" + serviceCensor.getServiceName() + "\"审核未通过";
                content = user.getUserName() + "您好！您添加的\"" + serviceCensor.getServiceName() + "\"服务未通过审核，原因是：" + msg;
            } else {
                subject = "更新服务\"" + serviceCensor.getServiceName() + "\"审核未通过";
                content = user.getUserName() + "您好！您更新的\"" + serviceCensor.getServiceName() + "\"服务未通过审核，原因是：" + msg;
            }

            result = sendSmsUtil.sendMsgByMail(user.getUserEmail(), subject, content);

        } else if (!user.getUserPhone().equals("")) {
            String[] params = new String[]{"-", "-", "-"};
            if (serviceCensor.getServiceCensorType() == 0) {
                params = new String[]{user.getUserName(), "添加服务审核", serviceCensor.getServiceName(), msg};
            } else {
                params = new String[]{user.getUserName(), "更新服务审核", serviceCensor.getServiceName(), msg};
            }
            result = sendSmsUtil.sendMsgByPhone(user.getUserPhone(), params, 1432335);
        }
        return result;
    }

//    public int updateService(ServiceCensor serviceCensor){
//        return serviceMapper.updateServiceByServiceId(serviceCensor);
//    }
}
