package com.calorie.service.order;


import cn.hutool.core.date.DateTime;
import com.calorie.mapper.order.ServiceOrderMapper;
import com.calorie.mapper.workout.ServiceMapper;
import com.calorie.model.FoodOrder;
import com.calorie.model.ServiceOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.List;

@Service
public class ServiceOrderService {
    @Autowired
    ServiceOrderMapper serviceOrderMapper;
    @Autowired
    ServiceMapper serviceMapper;

    public List<ServiceOrder> selectServiceOrderByShopIdServiceOrderStatus(Integer shopId, Integer foodOrderStatus) {
        List<ServiceOrder> serviceOrderList = serviceOrderMapper.selectServiceOrderByShopIdServiceOrderStatus(shopId, foodOrderStatus);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        for (ServiceOrder serviceOrder : serviceOrderList) {
            serviceOrder.setServiceOrderCreatedTimeFormatedStr(format.format(serviceOrder.getServiceOrderCreatedTime()));
            for (com.calorie.model.Service serv : serviceOrder.getServiceList()){
                serv.setServiceAppointmentTime(serviceOrderMapper.selectServiceServiceOrderAppointmentTime(serv.getServiceId(), serviceOrder.getServiceOrderId()));
            }
        }
        return serviceOrderList;
    }

    public ServiceOrder selectServiceOrderByServiceOrderId(Integer serviceOrderId) {
        ServiceOrder serviceOrder = serviceOrderMapper.selectServiceOrderByServiceOrderId(serviceOrderId);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        serviceOrder.setServiceOrderCreatedTimeFormatedStr(format.format(serviceOrder.getServiceOrderCreatedTime()));
        for (com.calorie.model.Service serv : serviceOrder.getServiceList()){
            serv.setServiceAppointmentTime(serviceOrderMapper.selectServiceServiceOrderAppointmentTime(serv.getServiceId(), serviceOrder.getServiceOrderId()));
        }
        return serviceOrder;
    }

    public int updateServiceOrderStatus(Integer serviceOrderId) {
        return serviceOrderMapper.updateServiceOrderStatus(serviceOrderId);

    }

    public List<ServiceOrder> selectAllServiceOrderByShopId(Integer shopId) {
        List<ServiceOrder> serviceOrderList = serviceOrderMapper.selectAllServiceOrderByShopId(shopId);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        for (ServiceOrder serviceOrder : serviceOrderList) {
            serviceOrder.setServiceOrderCreatedTimeFormatedStr(format.format(serviceOrder.getServiceOrderCreatedTime()));
            for (com.calorie.model.Service serv : serviceOrder.getServiceList()){
                serv.setServiceAppointmentTime(serviceOrderMapper.selectServiceServiceOrderAppointmentTime(serv.getServiceId(), serviceOrder.getServiceOrderId()));
            }
        }
        return serviceOrderList;
    }
    public void addNewServiceOrder(ServiceOrder serviceOrder){
        serviceOrder.setServiceOrderStatus(0);
        serviceOrder.setServiceOrderComment(null);
        serviceOrder.setServiceOrderCreatedTime(new DateTime());
        float totalCalorie = 0.0f;
        int totalTime = 0;
        for (com.calorie.model.Service serv : serviceOrder.getServiceList()) {
            totalCalorie += serviceMapper.selectServiceByServiceId(serv.getServiceId()).getServiceCalorie() * serv.getServiceAppointmentTime();
            totalTime += serv.getServiceAppointmentTime();
        }
        serviceOrder.setServiceOrderTotalCalorie(totalCalorie);
        serviceOrder.setServiceOrderTotalTime(totalTime);
        serviceOrderMapper.insertServiceOrder(serviceOrder);
        Integer serviceOrderId = serviceOrderMapper.getCurServiceOrderId();
        for (com.calorie.model.Service serv : serviceOrder.getServiceList())
        {
            serviceOrderMapper.insertServiceServiceOrder(serv.getServiceId(), serviceOrderId, serv.getServiceAppointmentTime());
        }
    }

    public void changeServiceOrderStatus(Integer serviceOrderId, Integer nextStatus){
        serviceOrderMapper.changeServiceOrderStatus(serviceOrderId, nextStatus);
    }
    public void updateServiceOrderComment(Integer serviceOrderId, String comment){
        serviceOrderMapper.updateServiceOrderComment(serviceOrderId, comment);
    }

    public List<ServiceOrder> queryServiceOrderByUserId(Integer userId){
        List<ServiceOrder> serviceOrderList =  serviceOrderMapper.selectAllServiceOrderByUserId(userId);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        for (ServiceOrder serviceOrder : serviceOrderList) {
            serviceOrder.setServiceOrderCreatedTimeFormatedStr(format.format(serviceOrder.getServiceOrderCreatedTime()));
            for (com.calorie.model.Service serv : serviceOrder.getServiceList()){
                serv.setServiceAppointmentTime(serviceOrderMapper.selectServiceServiceOrderAppointmentTime(serv.getServiceId(), serviceOrder.getServiceOrderId()));
            }
        }
        return serviceOrderList;
    }

}
