package com.calorie.model;

import cn.hutool.core.date.DateTime;
import com.calorie.common.dto.OrderUserDto;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class FoodOrder {
    private Integer foodOrderId;

    private Float foodOrderPrice;

    private Float foodOrderCalorie;

    private Integer foodOrderStatus;

    private String foodOrderLocation;

    private Boolean foodOrderRiderSend;

    private String foodOrderComment;

    private Integer foodOrderShopId;

    private Integer foodOrderUserId;

    private OrderUserDto user;

    private List<Food> foodList;

//    private List<FoodFoodOrder> foodFoodOrderFoodNumber;

    //foodIdAndNum[i][0]为foodId，foodIdAndNum[i][1]为对应的数量foodNumber
    List<List<Integer>> foodIdAndNum;
    private Date foodOrderCreatedTime;
    private String foodOrderCreatedTimeFormatedStr;

}