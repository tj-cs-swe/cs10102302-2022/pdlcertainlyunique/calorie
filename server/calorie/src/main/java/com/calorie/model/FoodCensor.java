package com.calorie.model;

import lombok.Data;

import java.util.Date;

@Data
public class FoodCensor {
    private Integer foodCensorId;

    private String foodName;

    private String foodIntroduction;

    private String foodIngredient;

    private String foodImage;

    private String foodCategory;

    private Float foodPrice;

    private Float foodNumber;

    private Float foodCalorie;

    private Date foodCensorCreateDate;

    private Integer foodShopId;

    private Integer foodCensorStatus;

    private Integer foodCensorType;

    private String foodCensorMsg;

    private Integer foodId;

    private Integer foodTakeStatus;

    private String foodCensorCreateDateFormat;
}