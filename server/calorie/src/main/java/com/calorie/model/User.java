package com.calorie.model;

import lombok.Data;

@Data
public class User {
    private Integer userId;

    private String userName;

    private String userPassword;

    private String userNickname;

    private String userPhone;

    private String userEmail;

    private Float userHeight;

    private Float userWeight;

    private Integer userAge;

    private String userSex;

    private Float userBmi;

    private Float userRee;

    /**
     * 0: 未认证
     * 1：未认证 + 待审核
     * 2：已认证 + 待审核 （修改）
     * 3：未认证 + 审核失败
     * 4: 已认证 + 审核失败 （修改）
     * 5: 认证成功
     */
    private Integer userIsFoodMerchant;

    private Integer userIsGymMerchant;

    private Integer userIsRider;

    private String userDrivingLicense;

    private String userHealthCertificate;

    private String userQualificationProof;

    private String userIdProof;

    /**
     * 0 卧床（全天）
     * 1 轻活动（多坐、缓步）
     * 2 一般活动度
     * 3 活动量大
     */
    private Integer userWorkoutQuantityPrefer;
    private String userRole;
}
