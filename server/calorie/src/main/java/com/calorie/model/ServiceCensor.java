package com.calorie.model;

import lombok.Data;

import java.util.Date;

@Data
public class ServiceCensor {
    private Integer serviceCensorId;

    private String serviceName;

    private String serviceIntroduction;

    private Float serviceCalorie;

    private String serviceTime;

    private String serviceLocation;

    private String serviceCategory;

    private Integer serviceShopId;

    private Date serviceCensorCreateDate;

    private Integer serviceCensorStatus;

    private Integer serviceCensorType;

    private String serviceImage;

    private String serviceCensorMsg;



    private Integer serviceId;

    private String serviceCensorCreateDateFormat;
}