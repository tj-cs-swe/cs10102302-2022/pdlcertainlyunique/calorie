package com.calorie.model;

import lombok.Data;

@Data
public class Service {
    private Integer serviceId;

    private String serviceName;

    private String serviceIntroduction;

    private Float serviceCalorie;

    private String serviceTime;

    private String serviceLocation;

    private Integer serviceShopId;

    private String serviceCategory;

    private String serviceImage;
    private Integer serviceAppointmentTime;
}