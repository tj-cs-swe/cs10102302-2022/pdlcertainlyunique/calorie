package com.calorie.model;

import lombok.Data;

import java.util.List;

@Data
public class Shop {
    private Integer shopId;

    private String shopName;

    private String shopIntroduction;

    private String shopLocation;

    private String shopImage;

    private String shopType;

    private List<Food> foodList;
    private List<Service> serviceList;
    private Integer userId;
}