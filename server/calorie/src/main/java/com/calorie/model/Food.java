package com.calorie.model;

import lombok.Data;

@Data
public class Food {
    private Integer foodId;

    private String foodName;

    private String foodIntroduction;

    private String foodImage;

    private String foodCategory;

    private String foodIngredient;

    private Float foodPrice;

    private Float foodNumber;

    private Float foodCalorie;

    private Integer foodTakeStatus;

    private Integer foodShopId;

    private Integer foodOrderNumber;
}