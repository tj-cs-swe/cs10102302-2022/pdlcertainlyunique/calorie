package com.calorie.model;

import com.calorie.common.dto.OrderUserDto;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class ServiceOrder {
    private Integer serviceOrderId;

    private String serviceOrderArrivalTime;

    private Integer serviceOrderStatus;

    private String serviceOrderComment;

    private Integer serviceOrderShopId;

    private Integer serviceOrderUserId;

    private OrderUserDto user;

    private List<Service> serviceList;
    private Date serviceOrderCreatedTime;
    private String serviceOrderCreatedTimeFormatedStr;

    private Float serviceOrderTotalCalorie;
    //健身订单总时长，以分钟为单位，因为service里的serviceCalorie是每分钟消耗卡路里数
    private Integer serviceOrderTotalTime;
}