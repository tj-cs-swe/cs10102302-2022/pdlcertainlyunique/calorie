package com.calorie;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.oas.annotations.EnableOpenApi;


@MapperScan("com.calorie.mapper") //扫描的mapper
@EnableOpenApi
@SpringBootApplication
public class CalorieApplication {

    public static void main(String[] args) {
        SpringApplication.run(CalorieApplication.class, args);
    }

}
