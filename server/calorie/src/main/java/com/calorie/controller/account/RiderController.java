package com.calorie.controller.account;

import com.calorie.common.dto.RiderRecognizeDto;
import com.calorie.common.lang.Result;
import com.calorie.model.User;
import com.calorie.service.account.AuthenticationService;
import com.calorie.service.account.UserService;
import com.calorie.util.JwtUtil;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;

@RestController
public class RiderController {
    @Autowired
    UserService userService;

    @Autowired
    AuthenticationService authenticationService;

    @RequiresAuthentication
    @PostMapping("/rider")
    @ApiOperation(value = "骑手提交审核，包括重新提交")
    public Result riderAuth(@RequestHeader(value = "Authorization", required = true) String jwt,
                            @Validated @RequestBody RiderRecognizeDto riderDto
                            ) throws MessagingException {
        JwtUtil jwtUtil = new JwtUtil();
        String userName = (String) jwtUtil.decode(jwt).get("username"); // 获取 jwt 令牌

        if (!userName.equals(riderDto.getUserName())) {
            return Result.fail("token 用户名与骑手用户名不匹配");
        }

        User rider = userService.selectByUserName(userName);

        int userIsRider = rider.getUserIsRider();

        if (userIsRider == 0 || userIsRider == 3 || userIsRider == 1) userIsRider = 1; // 未认证 + 待审核
        else if (userIsRider == 5 || userIsRider == 4 || userIsRider == 2) userIsRider = 2; // 已认证 + 待审核 （修改）

        int result = authenticationService.updateRiderAuthInfo(userIsRider, riderDto);
        if (result != 0) return Result.succ("认证申请成功");
        else return Result.fail("认证申请失败，未更新成功");
    }
}
