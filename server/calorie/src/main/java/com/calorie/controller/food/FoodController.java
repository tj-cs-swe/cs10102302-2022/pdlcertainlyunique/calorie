package com.calorie.controller.food;


import cn.hutool.core.map.MapUtil;
import com.calorie.common.lang.Result;
import com.calorie.model.Food;
import com.calorie.model.Shop;
import com.calorie.service.ShopService;
import com.calorie.service.food.FoodService;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class FoodController {

    @Autowired
    FoodService foodService;

    @Autowired
    ShopService shopService;

    //用户浏览所有店铺
    @RequiresAuthentication
    @GetMapping("/foodshops")
    public Result browseFoodShopUser() {
        List<Shop> shopList = foodService.selectShopByShopType("food");
        if (shopList == null) {
            return Result.fail("查询失败");
        }
        return Result.succ(MapUtil.builder().put("shopList", shopList).map());
    }

    //用户浏览商家所有餐品
    @RequiresAuthentication
    @GetMapping("/foodshops/{shopId}")
    public Result browseFoodUser(@PathVariable(value = "shopId") Integer shopId) {
        Shop shopFood = foodService.selectShopFoodsByShopIdFoodStatus(shopId, "food");
        if (shopFood == null) {
            return Result.fail("查询失败");
        }
        return Result.succ(MapUtil.builder()
                .put("shopFood", shopFood)
                .map()
        );
    }

    //用户浏览商家某一餐品
    @RequiresAuthentication
    @GetMapping("/foodshops/{shopId}/{foodId}")
    public Result browseFoodInfoUser(@PathVariable(value = "shopId") Integer shopId,
                                     @PathVariable(value = "foodId") Integer foodId) {
        Food food = foodService.selectFoodByFoodIdShopId(foodId, shopId);
        if (food == null) {
            return Result.fail("查询失败");
        }
        return Result.succ(MapUtil.builder()
                .put("food", food)
                .map()
        );
    }


}
