package com.calorie.controller.account;

import cn.hutool.core.map.MapUtil;
import com.calorie.common.dto.AuthAuditDto;
import com.calorie.common.lang.Result;
import com.calorie.model.User;
import com.calorie.service.account.AuthenticationService;
import com.calorie.service.account.UserService;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.util.List;

@RestController
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    UserService userService;

    @Autowired
    AuthenticationService authenticationService;

    @RequiresAuthentication
    @RequiresRoles("admin")
    @GetMapping("/riders")
    @ApiOperation(value = "获取所有有认证请求的骑手")
    public Result indexRiderAuthRequests() {
        List<User> riders = authenticationService.selectRiderAuthRequests();

        return Result.succ(MapUtil.builder()
                .put("riders", riders)
                .map());
    }

    @RequiresAuthentication
    @RequiresRoles("admin")
    @PostMapping("/riders")
    @ApiOperation(value = "骑手认证请求处理")
    public Result handleRiderAuthRequests(@Validated @RequestBody AuthAuditDto authAuditDto
    ) throws MessagingException {
        String userName = authAuditDto.getUserName();
        User user = userService.selectByUserName(userName);

        User rider = userService.selectByUserName(userName);
        int userIsRider = rider.getUserIsRider();

        /*
         * 0: 未认证
         * 1：未认证 + 待审核
         * 2：已认证 + 待审核 （修改）
         * 3：未认证 + 审核失败
         * 4: 已认证 + 审核失败 （修改）
         * 5: 认证成功
         */
        if (userIsRider != 1 && userIsRider != 2) {
            return Result.fail("该骑手未提交审核");
        }

        if (authAuditDto.getIsVerified() != 0) { // 审核通过
            userIsRider = 5;
            authenticationService.deleteUserRole(userName, "user");
            authenticationService.deleteUserRole(userName, "rider");
            authenticationService.addUserRole(userName, "rider");
            authenticationService.updateUserIsRider(userIsRider, userName);
            String result = authenticationService.sendAcceptAuthMsg(user, "rider");

        } else if (userIsRider != 5) { // 失败
            if (userIsRider == 1) userIsRider = 3; // 未认证过但失败
            else if (userIsRider == 2) userIsRider = 4; // 认证过但失败
            authenticationService.updateUserIsRider(userIsRider, userName);
            String result = authenticationService.sendRejectAuthMsg(user, "rider");

        } else return Result.fail("该骑手已通过审核");

        return Result.succ("骑手认证审核处理成功");
    }

    @RequiresAuthentication
    @RequiresRoles("admin")
    @GetMapping("/merchant/foodMerchants")
    @ApiOperation(value = "获取所有有认证请求的食品商家")
    public Result indexFoodMerchantAuthRequests() {
        List<User> foodMerchants = authenticationService.selectFoodMerchantAuthRequests();

        return Result.succ(MapUtil.builder()
                .put("foodMerchants", foodMerchants)
                .map());
    }

    @RequiresAuthentication
    @RequiresRoles("admin")
    @PostMapping("/merchant/foodMerchants")
    @ApiOperation(value = "食品商家认证请求处理")
    public Result handleFoodMerchantAuthRequests(@Validated @RequestBody AuthAuditDto authAuditDto
    ) throws MessagingException {
        String userName = authAuditDto.getUserName();
        User user = userService.selectByUserName(userName);
        User foodMerchant = userService.selectByUserName(userName);
        int userIsFoodMerchant = foodMerchant.getUserIsFoodMerchant();

        if (userIsFoodMerchant != 1 && userIsFoodMerchant != 2) {
            return Result.fail("该食品商家未提交审核");
        }

        if (authAuditDto.getIsVerified() != 0) { // 审核通过
            userIsFoodMerchant = 5;
            authenticationService.deleteUserRole(userName, "user");
            authenticationService.deleteUserRole(userName, "foodMerchant");
            authenticationService.addUserRole(userName, "foodMerchant");
            authenticationService.updateUserIsFoodMerchant(userIsFoodMerchant, userName);
            String result = authenticationService.sendAcceptAuthMsg(user, "foodMerchant");
        } else if (userIsFoodMerchant != 5) { // 失败
            if (userIsFoodMerchant == 1) userIsFoodMerchant = 3; // 未认证过但失败
            else if (userIsFoodMerchant == 2) userIsFoodMerchant = 4; // 认证过但失败
            authenticationService.updateUserIsFoodMerchant(userIsFoodMerchant, userName);
            String result = authenticationService.sendRejectAuthMsg(user, "foodMerchant");

        } else return Result.fail("该食品商家已通过审核");

        return Result.succ("食品商家认证审核处理成功");
    }

    @RequiresAuthentication
    @RequiresRoles("admin")
    @GetMapping("/merchant/gymMerchants")
    @ApiOperation(value = "获取所有有认证请求的健身商家")
    public Result indexGymMerchantAuthRequests() {
        List<User> gymMerchants = authenticationService.selectGymMerchantAuthRequests();

        return Result.succ(MapUtil.builder()
                .put("gymMerchants", gymMerchants)
                .map());
    }

    @RequiresAuthentication
    @RequiresRoles("admin")
    @PostMapping("/merchant/gymMerchants")
    @ApiOperation(value = "健身商家认证请求处理")
    public Result handleGymMerchantAuthRequests(@Validated @RequestBody AuthAuditDto authAuditDto
    ) throws MessagingException {
        String userName = authAuditDto.getUserName();
        User user = userService.selectByUserName(userName);

        User gymMerchant = userService.selectByUserName(userName);
        int userIsGymMerchant = gymMerchant.getUserIsGymMerchant();

        if (userIsGymMerchant != 1 && userIsGymMerchant != 2) {
            return Result.fail("该健身商家未提交审核");
        }

        if (authAuditDto.getIsVerified() != 0) { // 审核通过
            userIsGymMerchant = 5;
            authenticationService.deleteUserRole(userName, "user");
            authenticationService.deleteUserRole(userName, "gymMerchant");
            authenticationService.addUserRole(userName, "gymMerchant");
            authenticationService.updateUserIsGymMerchant(userIsGymMerchant, userName);
            String result = authenticationService.sendAcceptAuthMsg(user, "gymMerchant");

        } else if (userIsGymMerchant != 5) { // 失败
            if (userIsGymMerchant == 1) userIsGymMerchant = 3; // 未认证过但失败
            else if (userIsGymMerchant == 2) userIsGymMerchant = 4; // 认证过但失败
            authenticationService.updateUserIsGymMerchant(userIsGymMerchant, userName);
            String result = authenticationService.sendRejectAuthMsg(user, "gymMerchant");

        } else return Result.fail("该健身商家已通过审核");

        return Result.succ("健身商家认证审核处理成功");
    }
}
