package com.calorie.controller.workout;

import cn.hutool.core.map.MapUtil;
import com.calorie.common.lang.Result;
import com.calorie.model.ServiceCensor;
import com.calorie.model.User;
import com.calorie.service.account.UserService;
import com.calorie.service.workout.WorkoutService;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.text.SimpleDateFormat;
import java.util.List;

@RestController
public class AdminWorkoutController {
    @Autowired
    WorkoutService workoutService;


    @Autowired
    UserService userService;

    @RequiresAuthentication
    @RequiresRoles("admin")
    @GetMapping("/servicecensor")
    public Result browseServiceCensor() {
        List<ServiceCensor> serviceCensorList = workoutService.selectAllServiceCensor();
        if (serviceCensorList == null) {
            return Result.fail("查询失败");
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        for (int i = 0; i < serviceCensorList.size(); i++) {
            serviceCensorList.get(i).setServiceCensorCreateDateFormat(format.format(serviceCensorList.get(i).getServiceCensorCreateDate()));
        }
        return Result.succ(MapUtil.builder().put("serviceCensorList", serviceCensorList).map());
    }

    @RequiresAuthentication
    @RequiresRoles("admin")
    @GetMapping("/servicecensor/{serviceCensorId}")
    public Result browseOneServiceCensor(@PathVariable(value = "serviceCensorId") Integer serviceCensorId) {
        ServiceCensor serviceCensor = workoutService.selectServiceCensorByServiceId(serviceCensorId);
        if (serviceCensor == null) {
            return Result.fail("查询失败");
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        serviceCensor.setServiceCensorCreateDateFormat(format.format(serviceCensor.getServiceCensorCreateDate()));

        return Result.succ(MapUtil.builder()
                .put("serviceCensor", serviceCensor)
                .map()
        );
    }

    @RequiresAuthentication
    @RequiresRoles("admin")
    @PostMapping("/acceptService/{serviceCensorId}")
    public Result acceptNewService(@PathVariable(value = "serviceCensorId") Integer serviceCensorId) throws MessagingException {
        ServiceCensor serviceCensor = workoutService.selectServiceCensorByServiceId(serviceCensorId);

        int insertNum = workoutService.insertNewService(serviceCensorId);
        if (insertNum != 1) {
            return Result.fail("添加餐品失败");
        }
        int deleteNum = workoutService.deleteNewService(serviceCensorId);
        if (deleteNum != 1) {
            return Result.fail("删除失败");
        }

        Integer shopId = serviceCensor.getServiceShopId();
        User user = userService.selectUserByShopId(shopId);
        String result = workoutService.sendAcceptServiceMsg(serviceCensor, user);

        if (!"OK".equals(result)) {
            return Result.fail("信息发送失败");
        }

        return Result.succ(MapUtil.builder()
                .put("info", "success")
                .map()
        );
    }

    @RequiresAuthentication
    @RequiresRoles("admin")
    @PostMapping("/rejectService/{serviceCensorId}")
    public Result rejectService(@PathVariable(value = "serviceCensorId") Integer serviceCensorId,
                                @RequestBody ServiceCensor serviceCensor
    ) throws MessagingException {
        ServiceCensor serviceCensor1 = workoutService.selectServiceCensorByServiceId(serviceCensorId);

        serviceCensor.setServiceCensorId(serviceCensorId);
        serviceCensor.setServiceCensorStatus(2);
        int updateNum = workoutService.updateRejectServiceMsg(serviceCensor);
        if (updateNum != 1) {
            return Result.fail("更新失败");
        }
        Integer shopId = serviceCensor1.getServiceShopId();
        serviceCensor1.setServiceCensorMsg(serviceCensor.getServiceCensorMsg());
        User user = userService.selectUserByShopId(shopId);
        String result = workoutService.sendRejectServiceMsg(serviceCensor1, user);

        if (!"OK".equals(result)) {
            return Result.fail("信息发送失败");
        }

        return Result.succ(MapUtil.builder()
                .put("info", "success")
                .map()
        );
    }
}
