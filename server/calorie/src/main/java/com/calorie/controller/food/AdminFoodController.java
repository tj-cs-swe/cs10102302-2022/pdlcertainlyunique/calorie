package com.calorie.controller.food;


import cn.hutool.core.map.MapUtil;
import com.calorie.common.lang.Result;
import com.calorie.model.FoodCensor;
import com.calorie.model.User;
import com.calorie.service.account.UserService;
import com.calorie.service.food.FoodService;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.text.SimpleDateFormat;
import java.util.List;

@RestController
public class AdminFoodController {

    @Autowired
    FoodService foodService;

    @Autowired
    UserService userService;


    @RequiresAuthentication
    @RequiresRoles("admin")
    @GetMapping("/foodcensor")
    public Result browsefoodCensor() {
        List<FoodCensor> foodCensorList = foodService.selectAllFoodCensor();
        if (foodCensorList == null) {
            return Result.fail("查询失败");
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        for (int i = 0; i < foodCensorList.size(); i++) {
            foodCensorList.get(i).setFoodCensorCreateDateFormat(format.format(foodCensorList.get(i).getFoodCensorCreateDate()));
        }
        return Result.succ(MapUtil.builder().put("foodCensorList", foodCensorList).map());
    }

    @RequiresAuthentication
    @RequiresRoles("admin")
    @GetMapping("/foodcensor/{foodCensorId}")
    public Result browseOnefoodCensor(@PathVariable(value = "foodCensorId") Integer foodCensorId) {
        FoodCensor foodCensor = foodService.selectFoodCensorByFoodId(foodCensorId);
        if (foodCensor == null) {
            return Result.fail("查询失败");
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        foodCensor.setFoodCensorCreateDateFormat(format.format(foodCensor.getFoodCensorCreateDate()));

        return Result.succ(MapUtil.builder()
                .put("foodCensor", foodCensor)
                .map()
        );
    }


    @RequiresAuthentication
    @RequiresRoles("admin")
    @PostMapping("/acceptFood/{foodCensorId}")
    public Result acceptNewFood(@PathVariable(value = "foodCensorId") Integer foodCensorId) throws MessagingException {
        FoodCensor foodCensor = foodService.selectFoodCensorByFoodId(foodCensorId);
        int insertNum = foodService.insertNewFood(foodCensorId);
        if (insertNum != 1) {
            return Result.fail("添加餐品失败");
        }
        int deleteNum = foodService.deleteFoodCensorByFoodCensorId(foodCensorId);
        if (deleteNum != 1) {
            return Result.fail("删除失败");
        }

        Integer shopId = foodCensor.getFoodShopId();
        User user = userService.selectUserByShopId(shopId);
        String result = foodService.sendAcceptFoodMsg(foodCensor, user);

        if (!"OK".equals(result)) {
            return Result.fail("信息发送失败");
        }

        return Result.succ(MapUtil.builder()
                .put("info", "success")
                .map()
        );
    }

    @RequiresAuthentication
    @RequiresRoles("admin")
    @PostMapping("/rejectFood/{foodCensorId}")
    public Result rejectFood(@PathVariable(value = "foodCensorId") Integer foodCensorId,
                             @RequestBody FoodCensor foodCensor
    ) throws MessagingException {
        FoodCensor foodCensor1 = foodService.selectFoodCensorByFoodId(foodCensorId);
        foodCensor.setFoodCensorId(foodCensorId);
        foodCensor.setFoodCensorStatus(2);
        int updateNum = foodService.updateRejectFoodMsg(foodCensor);
        if (updateNum != 1) {
            return Result.fail("更新失败");
        }

        Integer shopId = foodCensor1.getFoodShopId();
        foodCensor1.setFoodCensorMsg(foodCensor.getFoodCensorMsg());
        User user = userService.selectUserByShopId(shopId);
        String result = foodService.sendRejectFoodMsg(foodCensor1, user);

        if (!"OK".equals(result)) {
            return Result.fail("信息发送失败");
        }


        return Result.succ(MapUtil.builder()
                .put("info", "success")
                .map()
        );
    }
}
