package com.calorie.controller;

import cn.hutool.core.map.MapUtil;
import com.calorie.common.lang.Result;
import com.calorie.model.Shop;
import com.calorie.model.User;
import com.calorie.service.ShopService;
import com.calorie.service.account.UserService;
import com.calorie.util.JwtUtil;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ShopController {
    @Autowired
    ShopService shopService;

    @Autowired
    UserService userService;

    @RequiresAuthentication
    @PostMapping("/newShop")
    public Result newShop(@RequestHeader(value = "Authorization", required = true) String jwt,
                          @RequestBody Shop shop) {
        JwtUtil jwtUtil = new JwtUtil();
        String userName = (String) jwtUtil.decode(jwt).get("username");
        User user = userService.selectByUserName(userName);

        int insertNum = shopService.insertNewShop(shop);

        if (insertNum != 1) {
            return Result.fail("创建失败");
        }
        int shopId = shop.getShopId();
        int userId = user.getUserId();
        insertNum = shopService.insertNewShopUser(shopId, userId);
        if (insertNum != 1) {
            return Result.fail("创建失败");
        }
        return Result.succ(MapUtil.builder()
                .put("info", "success")
                .map()
        );
    }
}
