package com.calorie.controller.food;

import cn.hutool.core.map.MapUtil;
import com.calorie.common.lang.Result;
import com.calorie.model.Food;
import com.calorie.model.FoodCensor;
import com.calorie.model.Shop;
import com.calorie.model.User;
import com.calorie.service.ShopService;
import com.calorie.service.account.UserService;
import com.calorie.service.food.FoodService;
import com.calorie.util.JwtUtil;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MerchantFoodController {
    @Autowired
    FoodService foodService;

    @Autowired
    ShopService shopService;

    @Autowired
    UserService userService;

    //商家浏览自己的所有店铺
    @RequiresAuthentication
    @RequiresRoles("foodMerchant")
    @GetMapping("/myfoodshops")
    public Result browseFoodShop(@RequestHeader(value = "Authorization", required = true) String jwt) {
        JwtUtil jwtUtil = new JwtUtil();
        String userName = (String) jwtUtil.decode(jwt).get("username");
        User user = userService.selectByUserName(userName);
        Integer userId = user.getUserId();
        List<Shop> shopList = foodService.selectShopByShopTypeUserId("food", userId);
        if (shopList == null) {
            return Result.fail("查询失败");
        }
        return Result.succ(MapUtil.builder().put("shopList", shopList).map());
    }

    //商家浏览商铺所有餐品
    @RequiresAuthentication
    @RequiresRoles("foodMerchant")
    @GetMapping("/myfoodshops/{shopId}")
    public Result browseFood(@RequestHeader(value = "Authorization", required = true) String jwt,
                             @PathVariable(value = "shopId") Integer shopId) {
        JwtUtil jwtUtil = new JwtUtil();
        String userName = (String) jwtUtil.decode(jwt).get("username");
        User user = userService.selectByUserName(userName);
        Integer userId = user.getUserId();
        Shop shop = shopService.selectShopByShopIdShopTypeUserId(shopId, "food", userId);
        if (shop == null) {
            return Result.fail("查询失败");
        }
        Shop shopFood = foodService.selectShopFoodsByShopId(shopId, "food");
        if (shopFood == null) {
            return Result.fail("查询失败");
        }
        return Result.succ(MapUtil.builder()
                .put("shopFood", shopFood)
                .map()
        );
    }

    //商家浏览店铺某一餐品
    @RequiresAuthentication
    @RequiresRoles("foodMerchant")
    @GetMapping("/myfoodshops/{shopId}/{foodId}")
    public Result browseFoodInfo(@RequestHeader(value = "Authorization", required = true) String jwt,
                                 @PathVariable(value = "shopId") Integer shopId,
                                 @PathVariable(value = "foodId") Integer foodId) {
        JwtUtil jwtUtil = new JwtUtil();
        String userName = (String) jwtUtil.decode(jwt).get("username");
        User user = userService.selectByUserName(userName);
        Integer userId = user.getUserId();
        Shop shop = shopService.selectShopByShopIdShopTypeUserId(shopId, "food", userId);
        if (shop == null) {
            return Result.fail("查询失败");
        }
        Food food = foodService.selectFoodByFoodIdShopId(foodId, shopId);
        if (food == null) {
            return Result.fail("查询失败");
        }
        return Result.succ(MapUtil.builder()
                .put("food", food)
                .map()
        );
    }

    //商家增加餐品
    @RequiresAuthentication
    @RequiresRoles("foodMerchant")
    @GetMapping("/myfoodshops/{shopId}/addFood")
    public Result addFoodForCensorGet(@RequestHeader(value = "Authorization", required = true) String jwt,
                                      @PathVariable(value = "shopId") Integer shopId) {
        JwtUtil jwtUtil = new JwtUtil();
        String userName = (String) jwtUtil.decode(jwt).get("username");
        User user = userService.selectByUserName(userName);
        Integer userId = user.getUserId();
        Shop shop = shopService.selectShopByShopIdShopTypeUserId(shopId, "food", userId);
        if (shop == null) {
            return Result.fail("查询失败");
        }
        return Result.succ(MapUtil.builder()
                .put("info", "success")
                .map()
        );
    }

    //商家增加餐品
    @RequiresAuthentication
    @RequiresRoles("foodMerchant")
    @PostMapping("/myfoodshops/{shopId}/addFood")
    public Result addFoodForCensor(@PathVariable(value = "shopId") Integer shopId,
                                   @Validated @RequestBody FoodCensor foodCensor) {
        foodCensor.setFoodCensorType(0);
        int insertNum = foodService.insertNewFoodForCensor(foodCensor, shopId);
        if (insertNum != 1) {
            return Result.fail("添加餐品失败");
        }
        return Result.succ(MapUtil.builder()
                .put("info", "success")
                .map()
        );
    }

    //商家更新餐品
    @RequiresAuthentication
    @RequiresRoles("foodMerchant")
    @GetMapping("/myfoodshops/{shopId}/{foodId}/updateFood")
    public Result updateFoodForCensorGet(@RequestHeader(value = "Authorization", required = true) String jwt,
                                         @PathVariable(value = "shopId") Integer shopId,
                                         @PathVariable(value = "foodId") Integer foodId) {
        JwtUtil jwtUtil = new JwtUtil();
        String userName = (String) jwtUtil.decode(jwt).get("username");
        User user = userService.selectByUserName(userName);
        Integer userId = user.getUserId();
        Shop shop = shopService.selectShopByShopIdShopTypeUserId(shopId, "food", userId);
        if (shop == null) {
            return Result.fail("查询失败");
        }
        Food food = foodService.selectFoodByFoodIdShopId(foodId, shopId);
        if (food == null) {
            return Result.fail("查询失败");
        }
        return Result.succ(MapUtil.builder()
                .put("info", "success")
                .map()
        );
    }

    //商家更新餐品
    @RequiresAuthentication
    @RequiresRoles("foodMerchant")
    @PostMapping("/myfoodshops/{shopId}/{foodId}/updateFood")
    public Result updateFoodForCensor(@PathVariable(value = "shopId") Integer shopId,
                                      @PathVariable(value = "foodId") Integer foodId,
                                      @Validated @RequestBody FoodCensor foodCensor) {
//        foodDto.setFoodId(foodId);
        foodCensor.setFoodCensorType(foodId);
        int updateNum = foodService.updateFoodForCensor(foodCensor, shopId);
        if (updateNum != 1) {
            return Result.fail("更新餐品失败");
        }
        return Result.succ(MapUtil.builder()
                .put("info", "success")
                .map()
        );
    }

    //商家上架/下架餐品
    @RequiresAuthentication
    @RequiresRoles("foodMerchant")
    @PostMapping("/myfoodshops/{shopId}")
    public Result updateFoodStatus(@PathVariable(value = "shopId") Integer shopId,
                                   @Validated @RequestBody Food food) {
        int updateNum = foodService.updateFoodTakeStatus(food.getFoodTakeStatus(), food.getFoodId());
        if (updateNum != 1) {
            return Result.fail("更新状态失败");
        }
        return Result.succ(MapUtil.builder()
                .put("info", "success")
                .map()
        );
    }
}
