package com.calorie.controller.account;

import com.calorie.common.dto.MerchantRecognizeDto;
import com.calorie.common.lang.Result;
import com.calorie.model.User;
import com.calorie.service.account.AuthenticationService;
import com.calorie.service.account.UserService;
import com.calorie.util.JwtUtil;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;

@RestController
@RequestMapping("/merchant")
public class MerchantController {
    @Autowired
    UserService userService;

    @Autowired
    AuthenticationService authenticationService;

    @RequiresAuthentication
    @PostMapping("/foodMerchant")
    @ApiOperation(value = "餐品商家提交审核, 包括重新提交")
    public Result foodMerchantAuth(@RequestHeader(value = "Authorization", required = true) String jwt,
                                   @Validated @RequestBody MerchantRecognizeDto merchantDto
                                ) throws MessagingException {
        JwtUtil jwtUtil = new JwtUtil();
        String userName = (String) jwtUtil.decode(jwt).get("username"); // 获取 jwt 令牌

        if (!userName.equals(merchantDto.getUserName())) {
            return Result.fail("token 用户名与骑手用户名不匹配");
        }

        User foodMerchant = userService.selectByUserName(userName);

        int userIsFoodMerchant = foodMerchant.getUserIsFoodMerchant();

        if (userIsFoodMerchant == 0 || userIsFoodMerchant == 3 || userIsFoodMerchant == 1) userIsFoodMerchant = 1; // 未认证 + 待审核
        else if (userIsFoodMerchant == 5 || userIsFoodMerchant == 4 || userIsFoodMerchant == 2) userIsFoodMerchant = 2; // 已认证 + 待审核 （修改）

        int result = authenticationService.updateFoodMerchantAuthInfo(userIsFoodMerchant, merchantDto);
        if (result != 0) return Result.succ("认证申请成功");
        else return Result.fail("认证申请失败，未更新成功");
    }

    @RequiresAuthentication
    @PostMapping("/gymMerchant")
    @ApiOperation(value = "健身商家提交审核, 包括重新提交")
    public Result gymMerchantAuth(@RequestHeader(value = "Authorization", required = true) String jwt,
                                  @Validated @RequestBody MerchantRecognizeDto merchantDto
                                ) throws MessagingException {
        JwtUtil jwtUtil = new JwtUtil();
        String userName = (String) jwtUtil.decode(jwt).get("username"); // 获取 jwt 令牌

        if (!userName.equals(merchantDto.getUserName())) {
            return Result.fail("token 用户名与骑手用户名不匹配");
        }

        User foodMerchant = userService.selectByUserName(userName);

        int userIsGymMerchant = foodMerchant.getUserIsGymMerchant();

        if (userIsGymMerchant == 0 || userIsGymMerchant == 3 || userIsGymMerchant == 1) userIsGymMerchant = 1; // 未认证 + 待审核
        else if (userIsGymMerchant == 5 || userIsGymMerchant == 4 || userIsGymMerchant == 2) userIsGymMerchant = 2; // 已认证 + 待审核 （修改）

        int result = authenticationService.updateGymMerchantAuthInfo(userIsGymMerchant, merchantDto);
        if (result != 0) return Result.succ("认证申请成功");
        else return Result.fail("认证申请失败，未更新成功");
    }
}
