package com.calorie.controller.order;

import cn.hutool.core.map.MapUtil;
import com.calorie.common.dto.FoodOrderDto;
import com.calorie.common.dto.MakeCommentsDto;
import com.calorie.common.dto.UserCommentsDto;
import com.calorie.common.lang.Result;
import com.calorie.model.Food;
import com.calorie.model.FoodOrder;
import com.calorie.service.ShopService;
import com.calorie.service.account.UserService;
import com.calorie.service.food.FoodService;
import com.calorie.service.order.FoodOrderService;
import com.calorie.service.recommend.RecommendService;
import com.calorie.util.JwtUtil;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.util.*;

@RestController
public class FoodOrderController {

    @Autowired
    FoodOrderService foodOrderService;

    @RequiresAuthentication
    @PostMapping("/myFoodOrders/{shopId}")
    public Result browseFoodOrderUser(@PathVariable(value = "shopId") Integer shopId,
                                      @RequestBody FoodOrder foodOrder) {
        List<FoodOrder> foodOrderList;
        Integer foodOrderStatus = foodOrder.getFoodOrderStatus();
        if (foodOrderStatus == -1) {
            foodOrderList = foodOrderService.selectAllFoodOrderByShopId(shopId);
        } else {
            foodOrderList = foodOrderService.selectFoodOrderByShopIdFoodOrderStatus(shopId, foodOrderStatus);
        }
        if (foodOrderList == null) {
            return Result.fail("查询失败");
        }
        return Result.succ(MapUtil.builder().put("foodOrderList", foodOrderList).map());
    }


    @RequiresAuthentication
    @GetMapping("/myFoodOrders/{shopId}/{foodOrderId}")
    public Result browseFoodOrderInfo(@PathVariable(value = "shopId") Integer shopId,
                                      @PathVariable(value = "foodOrderId") Integer foodOrderId) {
        FoodOrder foodOrder = foodOrderService.selectFoodOrderByFoodOrderId(foodOrderId);
        if (foodOrder == null) {
            return Result.fail("查询失败");
        }
        return Result.succ(MapUtil.builder()
                .put("foodOrder", foodOrder)
                .map()
        );
    }

    //商品商家接单
    @RequiresAuthentication
    @PostMapping("/myFoodOrders/acceptFoodOrder")
    public Result acceptFoodOrder(@RequestBody FoodOrder foodOrder) {
        Integer foodOrderId = foodOrder.getFoodOrderId();
        int updateNum = foodOrderService.updateFoodOrderStatus(foodOrderId);
        if (updateNum != 1) {
            return Result.fail("接单失败");
        }
        return Result.succ(MapUtil.builder()
                .put("info", "success")
                .map()
        );
    }


    @PostMapping("/usersFoodOrders/{userId}")
    @RequiresAuthentication
    public Result queryUsersFoodOrders(@PathVariable(value = "userId") Integer userId,
                                       @RequestBody FoodOrder foodOrder) {
        Integer foodOrderStatus = foodOrder.getFoodOrderStatus();
        List<FoodOrder> foodOrderList;
        if (foodOrderStatus == -1) {
            foodOrderList = foodOrderService.queryFoodOrderByUserId(userId);
        } else {
            foodOrderList = foodOrderService.queryFoodOrderByUserIdFoodOrderStatus(userId, foodOrderStatus);
        }
        if (foodOrderList == null) {
            return Result.fail("查询失败");
        }
        return Result.succ(MapUtil.builder().put("foodOrderList", foodOrderList).map());
    }

    @GetMapping("/ridersFoodOrders/{riderUserId}")
    @RequiresAuthentication
    public Result getAllFoodOrdersAssignedToSomeRider(@PathVariable(value = "riderUserId") Integer riderUserId) {
        List<FoodOrder> foodOrderList = foodOrderService.getAllFoodOrdersAssignedToSomeRider(riderUserId);
        if (foodOrderList.size() == 0){
            return Result.fail("当前骑手尚未被分配订单");
        }
        else{
            return Result.succ(MapUtil.builder().put("foodOrderList", foodOrderList).map());
        }
    }



    @PostMapping("/foodorder/new")
    @RequiresAuthentication
    public Result addNewFoodOrder(@Validated @RequestBody FoodOrderDto foodOrderDto) {
        FoodOrder foodOrder = new FoodOrder();
        foodOrder.setFoodOrderUserId(foodOrderDto.getUserId());
        foodOrder.setFoodOrderShopId(foodOrderDto.getShopId());
        foodOrder.setFoodIdAndNum(foodOrderDto.getFoodIdAndNum());
        foodOrder.setFoodOrderLocation(foodOrderDto.getFoodOrderLocation());
        List<Float> ret = foodOrderService.addNewFoodOrder(foodOrder);
        return Result.succ(
                MapUtil.builder()
                        .put("totalCalorie", ret.get(0))
                        .put("totalPrice", ret.get(1))
                        .map());
    }

    @GetMapping("/foodshops/{shopId}/comments")
    @RequiresAuthentication
    public Result queryCommentsByShopId(@PathVariable(value = "shopId") Integer shopId) {
        List<UserCommentsDto> foodOrderComments = foodOrderService.queryCommentsByShopId(shopId);
        if (foodOrderComments == null) {
            return Result.fail("此商家暂无评论");
        }
        return Result.succ(MapUtil.builder().put("foodOrderCommentsList", foodOrderComments).map());
    }


    @GetMapping("/foodshops/{foodOrderId}/comment")
    @RequiresAuthentication
    public Result queryCommentByFoodOrderId(@PathVariable(value = "foodOrderId") Integer foodOrderId) {
        String comment = foodOrderService.queryCommentByFoodOrderId(foodOrderId);
        if (comment == null) {
            return Result.fail("此订单暂无评论");
        }
        return Result.succ(MapUtil.builder()
                .put("comment", comment)
                .map());
    }

    @PostMapping("/foodorder/{foodOrderId}/confirmation/{nextStatus}")
    @RequiresAuthentication
    public Result confirmFoodOrder(@PathVariable(value = "foodOrderId") Integer foodOrderId,
                                   @PathVariable(value = "nextStatus") Integer nextStatus) {
        foodOrderService.confirmFoodOrder(foodOrderId, nextStatus);
        return Result.succ(MapUtil.builder()
                .put("curStatus", nextStatus)
                .map());
    }

    @PostMapping("/foodorder/{foodOrderId}/makeComments")
    @RequiresAuthentication
    public Result makeComments(@PathVariable(value = "foodOrderId") Integer foodOrderId,
                               @RequestBody MakeCommentsDto makeCommentsDto) {
        FoodOrder foodOrder = foodOrderService.queryFoodOrderByFoodOrderId(foodOrderId);
        if (!Objects.equals(foodOrder.getFoodOrderUserId(), makeCommentsDto.getUserId())) {
            return Result.fail("此订单非当前用户订单，无法评论");
        } else if (foodOrder.getFoodOrderStatus() < 3) {
            return Result.fail("此订单暂时无法进行评价");
        } else {
            foodOrderService.updateComment(foodOrderId, makeCommentsDto.getComment());
            foodOrderService.confirmFoodOrder(foodOrderId, 4);
            return Result.succ(null);
        }
    }

    @PostMapping("/foodorder/{foodOrderId}/deliveryMethod/{deliveryMethod}")
    @RequiresAuthentication
    public Result confirmDeliveryMethod(@PathVariable(value = "foodOrderId") Integer foodOrderId,
                                        @PathVariable(value = "deliveryMethod") Integer deliveryMethod) {
        foodOrderService.updateFoodOrderRiderSend(foodOrderId, deliveryMethod);
        return Result.succ(null);
    }

    @PostMapping("/foodorder/{foodOrderId}/assignToRider/{riderUserId}")
    @RequiresAuthentication
    public Result assignFoodOrderToRider(@PathVariable(value = "foodOrderId") Integer foodOrderId,
                                         @PathVariable(value = "riderUserId") Integer riderUserId) {
        Integer res = foodOrderService.assignFoodOrderToRider(foodOrderId, riderUserId);
        if (res == -1)
            return Result.fail("当前无可用骑手");
        else if (res == -2)
            return Result.fail("指派的骑手不合法");
        else
            return Result.succ(MapUtil.builder()
                    .put("ridersUserId", res)
                    .map());
    }

    @PostMapping("/foodorder/{foodOrderId}/getOrder/{nextStatus}")
    @RequiresAuthentication
    @RequiresRoles("rider")
    public Result riderGetOrder(@PathVariable(value = "foodOrderId") Integer foodOrderId,
                                @PathVariable(value = "nextStatus") Integer nextStatus) {
        Integer ret = foodOrderService.riderGetFoodOrder(foodOrderId, nextStatus);
        if (ret == -1) {
            return Result.fail("当前状态订单无法被骑手接单");
        }
        else {
            return Result.succ(MapUtil.builder()
                    .put("curStatus", nextStatus)
                    .map());
        }
    }





    // 以下为推荐
    @Autowired
    RecommendService recommendService;
    @Autowired
    UserService userService;
    @Autowired
    ShopService shopService;
    @Autowired
    FoodService foodService;

    @GetMapping("/foodorder/recommend")
    @RequiresAuthentication
    public Result recommendFood(@RequestHeader(value = "Authorization", required = true) String jwt
    ) throws MessagingException {
        JwtUtil jwtUtil = new JwtUtil();
        String userName = (String) jwtUtil.decode(jwt).get("username"); // 获取 jwt 令牌

        List<Integer> foodShopdIds = new ArrayList<>();
        for (Integer id : shopService.selectAllFoodShopId()) {
            if (foodService.selectFoodByShopIdStatus(id).size() != 0) {
                foodShopdIds.add(id); // 选择有食品的商家
            }
        }
        if (foodShopdIds.size() == 0) {
            return Result.fail("没有供推荐的食品，请等待食品商家上架食品");
        }

        Random rand = new Random(); // 随机数生成器
        int randId = foodShopdIds.get(Math.abs(rand.nextInt()) % foodShopdIds.size()); // 随机商家 id

        Map<Food, Integer> tmp = recommendService.foodRecommendation(userService.selectByUserName(userName), randId);

        if (tmp.isEmpty()) {
            return Result.fail("未找到适合您当日摄入卡路里下的食品推荐");
        }

        List<Food> foods = new ArrayList<>();
        List<Integer> foodsCount = new ArrayList<>();
        double totalCalorie = 0;

        for (Map.Entry<Food, Integer> entry : tmp.entrySet()) {
            foods.add(entry.getKey());
            foodsCount.add(entry.getValue());
            totalCalorie += entry.getKey().getFoodCalorie() * entry.getValue();
        }

        return Result.succ(MapUtil.builder()
                .put("shopId", randId)
                .put("foods", foods)
                .put("foodsCount", foodsCount)
                .put("totalCalorie", totalCalorie)
                .map());
    }
}
