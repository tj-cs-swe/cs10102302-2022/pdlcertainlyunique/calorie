package com.calorie.controller.order;

import cn.hutool.core.map.MapUtil;
import com.calorie.common.lang.Result;
import com.calorie.model.Service;
import com.calorie.model.ServiceOrder;
import com.calorie.service.ShopService;
import com.calorie.service.account.UserService;
import com.calorie.service.order.ServiceOrderService;
import com.calorie.service.recommend.RecommendService;
import com.calorie.service.workout.WorkoutService;
import com.calorie.util.JwtUtil;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

@RestController
public class ServiceOrderController {

    @Autowired
    ServiceOrderService serviceOrderService;

    @RequiresAuthentication
    @PostMapping("/myServiceOrders/{shopId}")
    public Result browseServiceOrderUser(@PathVariable(value = "shopId") Integer shopId,
                                         @RequestBody ServiceOrder serviceOrder) {
        Integer serviceOrderStatus = serviceOrder.getServiceOrderStatus();
        List<ServiceOrder> serviceOrderList;
        if (serviceOrderStatus == -1) {
            serviceOrderList = serviceOrderService.selectAllServiceOrderByShopId(shopId);
        } else {
            serviceOrderList = serviceOrderService.selectServiceOrderByShopIdServiceOrderStatus(shopId, serviceOrderStatus);
        }
        if (serviceOrderList == null) {
            return Result.fail("查询失败");
        }
        return Result.succ(MapUtil.builder().put("serviceOrderList", serviceOrderList).map());
    }


    @RequiresAuthentication
    @GetMapping("/myServiceOrders/{shopId}/{serviceOrderId}")
    public Result browseServiceOrderInfo(@PathVariable(value = "shopId") Integer shopId,
                                         @PathVariable(value = "serviceOrderId") Integer serviceOrderId) {
        ServiceOrder serviceOrder = serviceOrderService.selectServiceOrderByServiceOrderId(serviceOrderId);
        if (serviceOrder == null) {
            return Result.fail("查询失败");
        }
        return Result.succ(MapUtil.builder()
                .put("serviceOrder", serviceOrder)
                .map()
        );
    }

    //商品商家接单
    @RequiresAuthentication
    @PostMapping("/myServiceOrders/acceptServiceOrder")
    public Result acceptServiceOrder(@RequestBody ServiceOrder serviceOrder) {
        Integer serviceOrderId = serviceOrder.getServiceOrderId();
        int updateNum = serviceOrderService.updateServiceOrderStatus(serviceOrderId);
        if (updateNum != 1) {
            return Result.fail("接单失败");
        }
        return Result.succ(MapUtil.builder()
                .put("info", "success")
                .map()
        );
    }


    @RequiresAuthentication
    @PostMapping("/myServiceOrders/newServiceOrder")
    public Result addNewServiceOrder(@Validated @RequestBody ServiceOrder serviceOrder) {
        serviceOrderService.addNewServiceOrder(serviceOrder);
        return Result.succ(null);
    }

    @RequiresAuthentication
    @PostMapping("/myServiceOrders/confirmation")
    public Result changeServiceOrderStatus(@RequestBody ServiceOrder serviceOrder) {
        serviceOrderService.changeServiceOrderStatus(serviceOrder.getServiceOrderId(), serviceOrder.getServiceOrderStatus());
        return Result.succ(null);
    }

    @RequiresAuthentication
    @PostMapping("/myServiceOrders/comment")
    public Result updateServiceOrderComment(@RequestBody ServiceOrder serviceOrder) {
        serviceOrderService.updateServiceOrderComment(serviceOrder.getServiceOrderId(), serviceOrder.getServiceOrderComment());
        return Result.succ(null);
    }

    @RequiresAuthentication
    @GetMapping("/usersServiceOrders/{userId}")
    public Result queryServiceOrderByUserId(@PathVariable(value = "userId") Integer userId){
        List<ServiceOrder> serviceOrderList;
        serviceOrderList = serviceOrderService.queryServiceOrderByUserId(userId);
        if (serviceOrderList == null)
            return Result.fail("暂无数据");
        return Result.succ(MapUtil.builder().put("serviceOrderList", serviceOrderList).map());
    }







    // 以下为推荐
    @Autowired
    ShopService shopService;
    @Autowired
    WorkoutService workoutService;
    @Autowired
    RecommendService recommendService;
    @Autowired
    UserService userService;

    @GetMapping("/serviceorder/recommend")
    @RequiresAuthentication
    public Result recommendService(@RequestHeader(value = "Authorization", required = true) String jwt
    ) throws MessagingException {
        JwtUtil jwtUtil = new JwtUtil();
        String userName = (String) jwtUtil.decode(jwt).get("username"); // 获取 jwt 令牌

        List<Integer> serviceShopdIds = new ArrayList<>();
        for (Integer id : shopService.selectAllServiceShopId()) {
            if (workoutService.selectServiceByShopId(id).size() != 0) {
                serviceShopdIds.add(id); // 选择有食品的商家
            }
        }
        if (serviceShopdIds.size() == 0) {
            return Result.fail("没有供推荐的健身服务，请等待健身商家添加健身服务");
        }

        Random rand = new Random(); // 随机数生成器
        int randId = serviceShopdIds.get(Math.abs(rand.nextInt()) % serviceShopdIds.size()); // 随机商家 id

        Map<Service, Integer> tmp = recommendService.workoutRecommendation(userService.selectByUserName(userName), randId);

        if (tmp.size() == 0) {
            return Result.fail("无当前卡路里摄入下的健身服务推荐");
        }

        // 将推荐信息转为数组
        List<Service> workouts = new ArrayList<>();
        List<Integer> workoutTims = new ArrayList<>();
        List<Double> workoutCalorie = new ArrayList<>();

        for (Map.Entry<Service, Integer> entry : tmp.entrySet()) {
            workouts.add(entry.getKey());
            workoutTims.add(entry.getValue());
            workoutCalorie.add((double) entry.getKey().getServiceCalorie() * entry.getValue());
        }

        return Result.succ(MapUtil.builder()
                .put("shopId", randId)
                .put("services", workouts)
                .put("time", workoutTims)
                .put("calorie", workoutCalorie)
                .map());
    }
}
