package com.calorie.controller.account;

import cn.hutool.core.map.MapUtil;
import com.calorie.common.dto.EditAccNameDto;
import com.calorie.common.dto.EditAccPwdDto;
import com.calorie.common.dto.HealthInfoDto;
import com.calorie.common.dto.PreferDto;
import com.calorie.common.lang.Result;
import com.calorie.model.User;
import com.calorie.service.account.AccountInfoService;
import com.calorie.service.account.UserService;
import com.calorie.service.healthcalc.HealthCalcService;
import com.calorie.util.JwtUtil;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
public class AccountInfoController {
    @Autowired
    AccountInfoService accountInfoService;

    @PostMapping("/account/editName")
    @RequiresAuthentication
    public Result editAccountName(@Validated @RequestBody EditAccNameDto editAccNameDto) {
        if (editAccNameDto.getEt() == EditAccNameDto.editType.editNickName) {
            Assert.notNull(editAccNameDto.getName(), "新昵称不能为空");
            Integer status = accountInfoService.updateUserNickname(editAccNameDto.getName(), editAccNameDto.getUserId());
            return Result.succ
                    (
                            MapUtil.builder()
                                    .put("userId", editAccNameDto.getUserId())
                                    .put("newNickName", editAccNameDto.getName())
                                    .put("status", status)
                                    .map()
                    );
        } else if (editAccNameDto.getEt() == EditAccNameDto.editType.editUserName) {
            Assert.notNull(editAccNameDto.getName(), "新用户名不能为空");

            Assert.isNull(userService.selectByUserName(editAccNameDto.getName()), "用户名已存在");

            String oldname = accountInfoService.selectByUserId(editAccNameDto.getUserId()).getUserName();

            Integer status = accountInfoService.updateUserName(editAccNameDto.getName(), editAccNameDto.getUserId());

            // 重新生成 jwt
            User user = accountInfoService.selectByUserId(editAccNameDto.getUserId());
            JwtUtil jwtUtil = new JwtUtil();
            Map<String, Object> chaim = new HashMap<>();
            chaim.put("username", user.getUserName());
            String token = jwtUtil.encode(user.getUserName(), 30 * 24 * 60 * 60 * 1000, chaim);

            // 修改权限
            accountInfoService.updateChangeNameRole(oldname, user.getUserName());

            return Result.succ
                    (
                            MapUtil.builder()
                                    .put("userId", editAccNameDto.getUserId())
                                    .put("newName", editAccNameDto.getName())
                                    .put("status", status)
                                    .put("token", token)
                                    .map()
                    );
        } else {
            return Result.succ(null);
        }
    }

    @PostMapping("account/password")
    @RequiresAuthentication
    public Result editAccountPassword(@Validated @RequestBody EditAccPwdDto editAccPwdDto) {
        Assert.notNull(editAccPwdDto.getPassword(), "新密码不能为空");
        Integer status = accountInfoService.updateUserPassword(editAccPwdDto.getPassword(), editAccPwdDto.getUserId());
        return Result.succ
                (
                        MapUtil.builder()
                                .put("userId", editAccPwdDto.getUserId())
                                .put("status", status)
                                .map()
                );
    }

    /*************************************************************************************/
    @Autowired
    UserService userService;

    @RequiresAuthentication
    @GetMapping("/profile")
    @ApiOperation(value = "用户信息")
    public Result index(@RequestHeader(value = "Authorization", required = true) String jwt
    ) throws MessagingException {
        JwtUtil jwtUtil = new JwtUtil();
        String userName = (String) jwtUtil.decode(jwt).get("username"); // 获取 jwt 令牌

        User user = userService.selectByUserName(userName);

        List<String> userRoles = userService.selectRolesByUserName(userName);

        List<String> userFoodTypePrefer = userService.selectFoodPreferByUserName(userName);

        List<String> userServiceTypePrefer = userService.selectServicePreferByUserName(userName);

        return Result.succ(MapUtil.builder()
                .put("user", user)
                .put("roles", userRoles)
                .put("foodTypePrefer", userFoodTypePrefer)
                .put("serviceTypePrefer", userServiceTypePrefer)
                .map());
    }

    // 下为用户喜好部分
    @RequiresAuthentication
    @PostMapping("/addFoodPrefer")
    @ApiOperation(value = "添加食品类型喜好")
    public Result addFoodTypePrefer(@RequestHeader(value = "Authorization", required = true) String jwt,
                                    @Validated @RequestBody PreferDto preferDto
    ) throws MessagingException {
        JwtUtil jwtUtil = new JwtUtil();
        String userName = (String) jwtUtil.decode(jwt).get("username"); // 获取 jwt 令牌

        accountInfoService.deleteUserFoodTypePrefer(userName, preferDto.getPrefer());
        accountInfoService.addUserFoodTypePrefer(userName, preferDto.getPrefer());
        return Result.succ("食品类型喜好添加成功");
    }

    @RequiresAuthentication
    @PostMapping("/deleteFoodPrefer")
    @ApiOperation(value = "删除食品类型喜好")
    public Result deleteFoodTypePrefer(@RequestHeader(value = "Authorization", required = true) String jwt,
                                       @Validated @RequestBody PreferDto preferDto
    ) throws MessagingException {
        JwtUtil jwtUtil = new JwtUtil();
        String userName = (String) jwtUtil.decode(jwt).get("username"); // 获取 jwt 令牌

        accountInfoService.deleteUserFoodTypePrefer(userName, preferDto.getPrefer());
        return Result.succ("食品类型喜好删除成功");
    }

    @RequiresAuthentication
    @PostMapping("/addServicePrefer")
    @ApiOperation(value = "添加健身服务类型喜好")
    public Result addServiceTypePrefer(@RequestHeader(value = "Authorization", required = true) String jwt,
                                       @Validated @RequestBody PreferDto preferDto
    ) throws MessagingException {
        JwtUtil jwtUtil = new JwtUtil();
        String userName = (String) jwtUtil.decode(jwt).get("username"); // 获取 jwt 令牌

        accountInfoService.deleteUserServiceTypePrefer(userName, preferDto.getPrefer());
        accountInfoService.addUserServiceTypePrefer(userName, preferDto.getPrefer());
        return Result.succ("添加健身服务类型喜好成功");
    }

    @RequiresAuthentication
    @PostMapping("/deleteServicePrefer")
    @ApiOperation(value = "删除健身服务类型喜好")
    public Result deleteServiceTypePrefer(@RequestHeader(value = "Authorization", required = true) String jwt,
                                          @Validated @RequestBody PreferDto preferDto
    ) throws MessagingException {
        JwtUtil jwtUtil = new JwtUtil();
        String userName = (String) jwtUtil.decode(jwt).get("username"); // 获取 jwt 令牌

        accountInfoService.deleteUserServiceTypePrefer(userName, preferDto.getPrefer());
        return Result.succ("删除健身服务类型喜好成功");
    }

    @Autowired
    HealthCalcService healthCalcService;

    @RequiresAuthentication
    @PostMapping("/workoutQuantityPrefer")
    @ApiOperation(value = "用户活动量偏好")
    public Result workoutQuantityPrefer(@RequestHeader(value = "Authorization", required = true) String jwt,
                                        @Validated @RequestBody PreferDto preferDto
    ) throws MessagingException {
        JwtUtil jwtUtil = new JwtUtil();
        String userName = (String) jwtUtil.decode(jwt).get("username"); // 获取 jwt 令牌

        Assert.notNull(preferDto.getUserWorkoutQuantityPrefer(), "用户活动量不能为空");

        accountInfoService.updateWorkoutQuantityPreferByUserName(userName, preferDto.getUserWorkoutQuantityPrefer());
        return Result.succ("设置用户活动量偏好成功");
    }

    // 用户健康信息
    @RequiresAuthentication
    @PostMapping("/healthInfo")
    @ApiOperation(value = "用户健康信息")
    public Result healthInfo(@RequestHeader(value = "Authorization", required = true) String jwt,
                             @RequestBody HealthInfoDto healthInfoDto
    ) throws MessagingException {
        JwtUtil jwtUtil = new JwtUtil();
        String userName = (String) jwtUtil.decode(jwt).get("username"); // 获取 jwt 令牌

        User user = userService.selectByUserName(userName);

        user.setUserHeight(healthInfoDto.getUserHeight());
        user.setUserWeight(healthInfoDto.getUserWeight());
        user.setUserAge(healthInfoDto.getUserAge());
        user.setUserSex(healthInfoDto.getUserSex());
        user.setUserBmi((float) healthCalcService.calcUserBMI(user));
        user.setUserRee((float) healthCalcService.calcUserREE(user));

        accountInfoService.updateUserHealthInfo(user);

        return Result.succ("用户健康信息更新成功");
    }
}