package com.calorie.controller.account;

import cn.hutool.core.map.MapUtil;
import cn.hutool.crypto.SecureUtil;
import com.calorie.common.dto.LoginDto;
import com.calorie.common.lang.Result;
import com.calorie.model.User;
import com.calorie.service.account.UserService;
import com.calorie.util.JwtUtil;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class LoginController {

    @Autowired
    UserService userService;


    @PostMapping("/login")
    @ApiOperation(value = "登录")
    public Result login(@Validated @RequestBody LoginDto loginDto) {

        User user = userService.selectByUserName(loginDto.getUserName());
        // Assert.notNull(user, "用户不存在");
        if (user == null) {
            return Result.fail("用户名或密码不正确");
        }

        if (!user.getUserPassword().equals(SecureUtil.md5(loginDto.getUserPassword()))) {
            return Result.fail("用户名或密码不正确");
        }

        JwtUtil jwtUtil = new JwtUtil();
        Map<String, Object> chaim = new HashMap<>();
        chaim.put("username", user.getUserName());
        String token = jwtUtil.encode(user.getUserName(), 30 * 24 * 60 * 60 * 1000, chaim);

        List<String> userRoles = userService.selectRolesByUserName(user.getUserName());

        return Result.succ(MapUtil.builder()
                .put("user", user)
                .put("token", token)
                .put("roles", userRoles)
                .map()
        );
    }

    @RequiresAuthentication
    @PostMapping("/logout")
    public Result logout() {
        return Result.succ(null);
    }

    @RequestMapping(path = "/unauthorized/{message}")
    public Result unauthorized(@PathVariable String message) throws UnsupportedEncodingException {
        return Result.fail(message);
    }
}

