package com.calorie.controller.workout;

import cn.hutool.core.map.MapUtil;
import com.calorie.common.lang.Result;
import com.calorie.model.Service;
import com.calorie.model.ServiceCensor;
import com.calorie.model.Shop;
import com.calorie.model.User;
import com.calorie.service.ShopService;
import com.calorie.service.account.UserService;
import com.calorie.service.workout.WorkoutService;
import com.calorie.util.JwtUtil;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MerchantWorkoutController {
    @Autowired
    WorkoutService workoutService;

    @Autowired
    ShopService shopService;

    @Autowired
    UserService userService;

    //商家浏览自己所有商铺
    @RequiresAuthentication
    @GetMapping("/myworkoutshops")
    public Result browseWorkoutShop(@RequestHeader(value = "Authorization", required = true) String jwt) {
        JwtUtil jwtUtil = new JwtUtil();
        String userName = (String) jwtUtil.decode(jwt).get("username");
        User user = userService.selectByUserName(userName);
        Integer userId = user.getUserId();
        List<Shop> shopList = workoutService.selectShopByShopTypeUserId("workout", userId);
        if (shopList == null) {
            return Result.fail("查询失败");
        }
        return Result.succ(MapUtil.builder().put("shopList", shopList).map());
    }

    //商家浏览自己商铺的所有服务
    @RequiresAuthentication
    @GetMapping("/myworkoutshops/{shopId}")
    public Result browseWorkout(@RequestHeader(value = "Authorization", required = true) String jwt,
                                @PathVariable(value = "shopId") Integer shopId) {
        JwtUtil jwtUtil = new JwtUtil();
        String userName = (String) jwtUtil.decode(jwt).get("username");
        User user = userService.selectByUserName(userName);
        Integer userId = user.getUserId();
        Shop shop = shopService.selectShopByShopIdShopTypeUserId(shopId, "workout", userId);
        if (shop == null) {
            return Result.fail("查询失败");
        }
        Shop shopServices = workoutService.selectShopServicesByShopId(shopId, "workout");
        if (shopServices == null) {
            return Result.fail("查询失败");
        }
        return Result.succ(MapUtil.builder()
                .put("shopServices", shopServices)
                .map()
        );
    }

    //商家浏览自己商铺的所有服务
    @RequiresAuthentication
    @GetMapping("/myworkoutshops/{shopId}/{serviceId}")
    public Result browseServiceInfo(@RequestHeader(value = "Authorization", required = true) String jwt,
                                    @PathVariable(value = "shopId") Integer shopId,
                                    @PathVariable(value = "serviceId") Integer serviceId) {
        JwtUtil jwtUtil = new JwtUtil();
        String userName = (String) jwtUtil.decode(jwt).get("username");
        User user = userService.selectByUserName(userName);
        Integer userId = user.getUserId();
        Shop shop = shopService.selectShopByShopIdShopTypeUserId(shopId, "workout", userId);
        if (shop == null) {
            return Result.fail("查询失败");
        }
        Service service = workoutService.selectServiceByServiceIdShopId(serviceId, shopId);
        if (service == null) {
            return Result.fail("查询失败");
        }
        return Result.succ(MapUtil.builder()
                .put("service", service)
                .map()
        );
    }

    //商家添加服务
    @RequiresAuthentication
    @GetMapping("/myworkoutshops/{shopId}/addService")
    public Result addServiceCensorGet(@RequestHeader(value = "Authorization", required = true) String jwt,
                                      @PathVariable(value = "shopId") Integer shopId) {
        JwtUtil jwtUtil = new JwtUtil();
        String userName = (String) jwtUtil.decode(jwt).get("username");
        User user = userService.selectByUserName(userName);
        Integer userId = user.getUserId();
        Shop shop = shopService.selectShopByShopIdShopTypeUserId(shopId, "workout", userId);
        if (shop == null) {
            return Result.fail("查询失败");
        }
        return Result.succ(MapUtil.builder()
                .put("info", "success")
                .map()
        );
    }

    //商家添加服务
    @RequiresAuthentication
    @PostMapping("/myworkoutshops/{shopId}/addService")
    public Result addServiceCensor(@PathVariable(value = "shopId") Integer shopId,
                                   @Validated @RequestBody ServiceCensor serviceCensor) {
        serviceCensor.setServiceCensorType(0);
        int insertNum = workoutService.insertNewServiceForCensor(serviceCensor, shopId);
        if (insertNum != 1) {
            return Result.fail("添加服务失败");
        }
        return Result.succ(MapUtil.builder()
                .put("info", "success")
                .map()
        );
    }

    //商家更新服务
    @RequiresAuthentication
    @GetMapping("/myworkoutshops/{shopId}/{serviceId}/updateService")
    public Result updateServiceCensorGet(@RequestHeader(value = "Authorization", required = true) String jwt,
                                         @PathVariable(value = "shopId") Integer shopId,
                                         @PathVariable(value = "serviceId") Integer serviceId) {
        JwtUtil jwtUtil = new JwtUtil();
        String userName = (String) jwtUtil.decode(jwt).get("username");
        User user = userService.selectByUserName(userName);
        Integer userId = user.getUserId();
        Shop shop = shopService.selectShopByShopIdShopTypeUserId(shopId, "workout", userId);
        if (shop == null) {
            return Result.fail("查询失败");
        }
        Service service = workoutService.selectServiceByServiceIdShopId(serviceId, shopId);
        if (service == null) {
            return Result.fail("查询失败");
        }
        return Result.succ(MapUtil.builder()
                .put("info", "success")
                .map()
        );
    }

    //商家更新服务
    @RequiresAuthentication
    @PostMapping("/myworkoutshops/{shopId}/{serviceId}/updateService")
    public Result updateServiceCensor(@PathVariable(value = "shopId") Integer shopId,
                                      @PathVariable(value = "serviceId") Integer serviceId,
                                      @Validated @RequestBody ServiceCensor serviceCensor) {
//        serviceDto.setServiceId(serviceId);
        serviceCensor.setServiceCensorType(serviceId);

        int updateNum = workoutService.updateServiceForCensor(serviceCensor, shopId);
        if (updateNum != 1) {
            return Result.fail("更新服务失败");
        }
        return Result.succ(MapUtil.builder()
                .put("info", "success")
                .map()
        );
    }
}
