package com.calorie.controller.workout;


import cn.hutool.core.map.MapUtil;
import com.calorie.common.lang.Result;
import com.calorie.model.Service;
import com.calorie.model.Shop;
import com.calorie.service.workout.WorkoutService;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class WorkoutController {
    @Autowired
    WorkoutService workoutService;

    //用户浏览所有健身商家
    @RequiresAuthentication
    @GetMapping("/workoutshops")
    public Result browseWorkoutShopUser() {
        List<Shop> shopList = workoutService.selectShopByShopType("workout");
        if (shopList == null) {
            return Result.fail("查询失败");
        }
        return Result.succ(MapUtil.builder().put("shopList", shopList).map());
    }

    //用户浏览健身商家所有服务
    @RequiresAuthentication
    @GetMapping("/workoutshops/{shopId}")
    public Result browseServiceUser(@PathVariable(value = "shopId") Integer shopId) {
        Shop shopServices = workoutService.selectShopServicesByShopId(shopId, "workout");
        if (shopServices == null) {
            return Result.fail("查询失败");
        }
        return Result.succ(MapUtil.builder()
                .put("shopServices", shopServices)
                .map()
        );
    }

    //用户浏览健身商家某一服务
    @RequiresAuthentication
    @GetMapping("/workoutshops/{shopId}/{serviceId}")
    public Result browseServiceInfoUser(@PathVariable(value = "shopId") Integer shopId,
                                        @PathVariable(value = "serviceId") Integer serviceId) {
        Service service = workoutService.selectServiceByServiceIdShopId(serviceId, shopId);
        if (service == null) {
            return Result.fail("查询失败");
        }
        return Result.succ(MapUtil.builder()
                .put("service", service)
                .map()
        );
    }


}
