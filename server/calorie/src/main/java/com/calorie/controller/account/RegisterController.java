package com.calorie.controller.account;

import com.calorie.common.dto.RegisterDto;
import com.calorie.common.lang.Result;
import com.calorie.model.User;
import com.calorie.service.account.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.MessagingException;

@RestController
public class RegisterController {

    @Autowired
    UserService userService;


    @PostMapping("/register")
    public Result register(@Validated @RequestBody RegisterDto registerDto) throws MessagingException {

        User user = userService.selectByUserName(registerDto.getUserName());
        Assert.isNull(user, "用户已存在");

        //注册方式： 0：手机号，1：邮箱
        int mode = -1;
        //手机号非空
        if(registerDto.getUserPhone()!=null){
            mode = 0;
        }
        //邮箱非空
        else if(registerDto.getUserEmail()!=null){
            mode = 1;
        }
        Result result = Result.fail("注册异常");;
        if(mode == 0){
            result = userService.phoneRegisterService(registerDto);
        }
        else if(mode == 1){
            result = userService.emailRegisterService(registerDto);
        }
        return result;
    }

}
