package com.calorie.controller.account;

import cn.hutool.core.map.MapUtil;
import com.calorie.common.dto.ForgetPwdDto;
import com.calorie.common.lang.Result;
import com.calorie.model.User;
import com.calorie.service.account.AccountInfoService;
import com.calorie.service.account.UserService;
import com.calorie.util.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.MessagingException;

@RestController
public class ForgetPwdController {
    //type 2 phone number,1 email
    //status 1 sent code 2 submit 3 edit password
    @Autowired
    RedisUtil redisUtil;
    @Autowired
    AccountInfoService accountInfoService;
    @Autowired
    UserService userService;

    @PostMapping("/password/new")
    public Result forgetPassword(@Validated @RequestBody ForgetPwdDto forgetPwdDto) throws MessagingException {
        User user = userService.selectByUserName(forgetPwdDto.getUserName());
        if (user == null) {
            return Result.fail("该用户名不存在");
        }
        if (forgetPwdDto.getStatus().equals("1")) {
            if (forgetPwdDto.getType() == 2) {
                if (!forgetPwdDto.getConnection().equals(user.getUserPhone()))
                    return Result.fail("用户名和手机号不匹配");
                else {
                    String code = userService.sendPhoneCode(forgetPwdDto.getConnection());
                    if (code.equals("OK"))
                        return Result.succ("验证码发送成功");
                    else
                        return Result.fail("验证码发送失败");
                }
            }
            if (forgetPwdDto.getType() == 1) {
                if (!forgetPwdDto.getConnection().equals(user.getUserEmail()))
                    return Result.fail("用户名和邮箱不匹配");
                else {
                    String code = userService.sendEmailCode(forgetPwdDto.getConnection());
                    if (code.equals("OK"))
                        return Result.succ("验证码发送成功");
                    else
                        return Result.fail("验证码发送失败");
                }
            }
        } else if (forgetPwdDto.getStatus().equals("2")) {
            if (forgetPwdDto.getType() == 2) {
                String phone = forgetPwdDto.getConnection();
                String code = redisUtil.get(phone);
                Assert.notNull(code, "验证码不存在，可能已失效");

                if (!code.equals(forgetPwdDto.getCode())) {
                    return Result.fail("验证码错误");
                }
                return Result.succ("验证成功");
            }
            if (forgetPwdDto.getType() == 1) {
                String email = forgetPwdDto.getConnection();
                String code = redisUtil.get(email);
                Assert.notNull(code, "验证码不存在，可能已失效");

                if (!code.equals(forgetPwdDto.getCode())) {
                    return Result.fail("验证码错误");
                }
                return Result.succ("验证成功");
            }
        } else if (forgetPwdDto.getStatus().equals("3")) {
            String code = redisUtil.get(forgetPwdDto.getConnection());
            if (code == null || !code.equals(forgetPwdDto.getCode())) {
                if (code != null)
                    redisUtil.del(forgetPwdDto.getConnection());
                return Result.fail("验证码已失效");
            }
            Integer status = accountInfoService.updateUserPassword(forgetPwdDto.getPassword(), user.getUserId());
            redisUtil.del(forgetPwdDto.getConnection());
            return Result.succ
                    (
                            MapUtil.builder()
                                    .put("userId", user.getUserId())
                                    .put("newPwd", forgetPwdDto.getPassword())
                                    .put("status", status)
                                    .put("msg", "密码修改成功")
                                    .map()
                    );
        } else {
            return Result.succ("未定义状态");
        }
        return Result.succ("null");
    }
}
