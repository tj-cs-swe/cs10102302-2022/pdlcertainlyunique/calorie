import Vue from 'vue'
import Router from 'vue-router'
import Homepage from '@/views/dashboard'
import Login from '@/views/login'
import Forgetpassword from '@/views/forgetpassword'
import Register from '@/views/register'
import ChangeHealthInfo from '@/views/changehealthinfo'
import ShowFoodShopComments from '@/views/showfoodshopcomments'
import userFavorSet from '@/views/userFavorSet'
import Riderrecognize from '../views/riderrecognize'
import shoprecognize from '../views/shoprecognize'
import ShowViewInfo from '../views/showViewInfo'
import BrowseFoodByShopID from '@/views/BrowseFoodByShopID'
import BrowseServiceByShopID from '@/views/BrowseServiceByShopID'
import AdminRecognize from '../views/AdminRecognize'
import addfood from '../views/addfood'
import addservice from '../views/addservice'
import modifyfood from '../views/modifyfood'
import modifyservice from '../views/modifyservice'
import UserFoodOrderManage from '../views/UserFoodOrderManager'
import UseSportOrderManage from '../views/UserSportOrderManager'
import MerFoodOrderManage from '../views/MerFoodOrderManager'
import RiderFoodOrderManage from '../views/RiderFoodOrderManager.vue'
import browsefood from '../views/browsefood'
import browseworkout from '../views/browseworkout'
import informationModification from '../views/informationModification'
import browseservices from '../views/browseservices'
import browseMyfoods from '../views/BrowseMyShop.vue'
import ServiceRecognize from '../views/ServiceRecognize.vue'
import BrowseMyService from '../views/BrowseMyService.vue'

import newshop from '../views/newshop'
import browsemyshops from '../views/browsemyshops'
Vue.use(Router)
const originalPush = Router.prototype.push

Router.prototype.push = function push (location) {
  return originalPush.call(this, location).catch(err => err)
}

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Homepage',
      component: Homepage
    }, {
      path: '/login',
      name: 'Login',
      component: Login
    }, {
      path: '/forgetpassword',
      name: 'Forgetpassword',
      component: Forgetpassword
    }, {
      path: '/register',
      name: 'Register',
      component: Register
    }, {
      path: '/riderrecognize',
      name: 'Riderrecognize',
      component: Riderrecognize
    }, {
      path: '/shoprecognize',
      name: 'shoprecognize',
      component: shoprecognize
    }, {
      path: '/showviewinfo',
      name: 'Showviewinfo',
      component: ShowViewInfo
    }, {
      path: '/browsefood/:id',
      name: 'BrowseFoodByShopID',
      component: BrowseFoodByShopID
    }, {
      path: '/browseService/:id',
      name: 'BrowseServiceByShopID',
      component: BrowseServiceByShopID
    }, {
      path: '/adminRecognize',
      name: 'AdminRecognize',
      component: AdminRecognize
    }, {
      path: '/:shopId/addfood',
      name: 'addfood',
      component: addfood
    }, {
      path: '/:shopId/addservice',
      name: 'addservice',
      component: addservice
    }, {
      path: '/modifyfood/:shopId/:foodId',
      name: 'modifyfood',
      component: modifyfood
    }, {
      path: '/modifyservice/:shopId/:serviceId',
      name: 'modifyservice',
      component: modifyservice
    }, {
      path: '/changehealthinfo',
      name: 'changehealthinfo',
      component: ChangeHealthInfo
    }, {
      path: '/UserFoodOrderManage',
      name: 'UserFoodOrderManage',
      component: UserFoodOrderManage
    }, {
      path: '/UseSportOrderManage',
      name: 'UseSportOrderManage',
      component: UseSportOrderManage
    }, {
      path: '/MerFoodOrderManage/:id',
      name: 'MerFoodOrderManage',
      component: MerFoodOrderManage
    }, {
      path: '/foodshops/:id/comments',
      name: 'showfoodshopcomments',
      component: ShowFoodShopComments
    }, {
      path: '/browsefood',
      name: 'browsefood',
      component: browsefood
    }, {
      path: '/browseworkout',
      name: 'browseworkout',
      component: browseworkout
    }, {
      path: '/informationModification',
      name: 'informationModification',
      component: informationModification
    }, {
      path: '/browseservices/:id',
      name: 'browseservices',
      component: browseservices
    }, {
      path: '/newshop',
      name: 'newshop',
      component: newshop
    }, {
      path: '/userFavorSet',
      name: 'userFavorSet',
      component: userFavorSet
    }, {
      path: '/browsemyshop/:id',
      name: 'browseMyfoods',
      component: browseMyfoods
    }, {
      path: '/browsemyshops',
      name: 'browsemyshops',
      component: browsemyshops
    }, {
      path: '/ServiceRecognize',
      name: 'ServiceRecognize',
      component: ServiceRecognize
    }, {
      path: '/BrowseMyService/:id',
      name: 'BrowseMyService',
      component: BrowseMyService
    }, {
      path: '/RiderFoodOrderManage/:id',
      name: 'RiderFoodOrderManage',
      component: RiderFoodOrderManage
    }
  ]
})
