// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import Element from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import './assets/css/all.css'
import store from './store'
import router from './router'
import axios from 'axios'

Vue.prototype.$axios = axios

Vue.use(Element)
Vue.config.productionTip = false
require('./mock')
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
