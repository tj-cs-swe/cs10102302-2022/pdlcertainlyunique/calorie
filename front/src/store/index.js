import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import tab from './tab'

Vue.use(Vuex)

export default new Vuex.Store({
  strict: true,
  plugins: [
    createPersistedState()
  ],
  state: {
    token: '',
    userInfo: JSON.parse(sessionStorage.getItem('userInfo')),
    recommend: JSON.parse(sessionStorage.getItem('recommend')),
    rolses: JSON.parse(sessionStorage.getItem('roles'))
  },
  mutations: {
    // set
    SET_TOKEN: (state, token) => {
      state.token = token
      localStorage.setItem('token', token)
    },
    SET_USERINFO: (state, userInfo) => {
      state.userInfo = userInfo
      sessionStorage.setItem('userInfo', JSON.stringify(userInfo))
    },
    SET_ROLES: (state, roles) => {
      state.roles = roles
      sessionStorage.setItem('roles', JSON.stringify(roles))
    },
    REMOVE_INFO: (state) => {
      state.token = ''
      state.userInfo = {}
      state.roles = {}
      localStorage.setItem('token', '')
      sessionStorage.setItem('userInfo', JSON.stringify(''))
    },
    SET_RECOMMEND: (state, recommend) => {
      state.recommend = recommend
      sessionStorage.setItem('recommend', JSON.stringify(recommend))
    },
    REMOVE_RECOMMEND: (state) => {
      state.recommend = {}
      sessionStorage.setItem('recommend', JSON.stringify(''))
    }
  },
  getters: {
    // get
    getUser: state => {
      return state.userInfo
    },
    getToken: state => {
      return state.token
    },
    getRoles: state => {
      return state.roles
    },
    getRecomend: state => {
      return JSON.parse(sessionStorage.getItem('recommend'))
    }
  },
  actions: {
  },
  modules: {
    tab
  }
})
