import Api from '@/services/Api'
export default {
  postfood (formName, config) {
    return Api().post('merchant/foodMerchant', formName, config)
  },
  postgym (formName, config) {
    return Api().post('merchant/gymMerchant', formName, config)
  }
}
