import Api from '@/services/Api'

export default {
  getfoodshop (config) {
    return Api().get('myfoodshops', config)
  },
  getworkoutshop (config) {
    return Api().get('myworkoutshops', config)
  }
}
