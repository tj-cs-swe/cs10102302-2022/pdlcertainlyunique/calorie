import Api from '@/services/Api'

export default {
  changeinfo (formName, config) {
    return Api().post('healthInfo', formName, config)
  },
  setQuantityPrefer (formName, config) {
    return Api().post('workoutQuantityPrefer', formName, config)
  }
}
