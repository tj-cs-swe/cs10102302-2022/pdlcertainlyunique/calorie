import Api from '@/services/Api'

export default {
  getRiders (config) {
    return Api().get('admin/riders', config)
  },
  getFoods (config) {
    return Api().get('admin/merchant/foodMerchants', config)
  },
  getFoodService (config) {
    return Api().get('foodcensor', config)
  },
  getGymService (config) {
    return Api().get('servicecensor', config)
  },
  getGyms (config) {
    return Api().get('admin/merchant/gymMerchants', config)
  },
  RiderVerify (formData, config) {
    return Api().post('admin/riders', formData, config)
  },
  FoodVerify (formData, config) {
    return Api().post('/admin//merchant/foodMerchants', formData, config)
  },
  GymVerify (formData, config) {
    return Api().post('/admin//merchant/gymMerchants', formData, config)
  },
  FoodServiceAccept (foodId, config) {
    console.log('food accept')
    console.log(config)
    return Api().post('/acceptFood/' + foodId, {}, config)
  },
  GymServiceAccept (Id, config) {
    return Api().post('acceptService/' + Id, {}, config)
  },
  FoodServiceReject (Id, formData, config) {
    return Api().post('rejectFood/' + Id, formData, config)
  },
  GymServiceReject (Id, formData, config) {
    return Api().post('rejectService/' + Id, formData, config)
  }
}
