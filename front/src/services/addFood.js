import Api from '@/services/Api'

export default {
  postfoodapply (formName, config, arg) {
    let path = 'myfoodshops/'
    path += arg
    path += '/addFood'
    return Api().post(path, formName, config)
  },
  getfoodapply (config, arg) {
    let path = 'myfoodshops/'
    path += arg
    path += '/addFood'
    return Api().get(path, config)
  }
}
