import Api from '@/services/Api'

export default {
  postserviceapply (formName, config, arg) {
    let path = 'myworkoutshops/'
    path += arg
    path += '/addService'
    return Api().post(path, formName, config)
  },
  getserviceapply (config, arg) {
    let path = 'myworkoutshops/'
    path += arg
    path += '/addService'
    return Api().get(path, config)
  }
}
