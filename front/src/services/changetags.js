import Api from '@/services/Api'

export default {
  foodAdd (formName, config) {
    return Api().post('addFoodPrefer', formName, config)
  },
  foodDelete (formName, config) {
    return Api().post('deleteFoodPrefer', formName, config)
  },
  sportAdd (formName, config) {
    return Api().post('addServicePrefer', formName, config)
  },
  sportDelete (formName, config) {
    return Api().post('deleteServicePrefer', formName, config)
  }
}
