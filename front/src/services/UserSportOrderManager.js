import Api from '@/services/Api'

export default {
  getOrders (userId, config) {
    console.log('getOrders')
    return Api().get('usersServiceOrders/' + userId, config)
  },
  UserComment (Form, config) {
    console.log('getOrders')
    return Api().post('myServiceOrders/comment', Form, config)
  }
}
