import Api from '@/services/Api'

export default {
  getInfo (config) {
    return Api().get('profile', config)
  }
}
