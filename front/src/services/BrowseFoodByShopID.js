import Api from '@/services/Api'

export default {
  getShopGoodList (shopId, config) {
    return Api().get('foodshops/' + shopId, config)
  },
  Order (formName, config) {
    return Api().post('foodorder/new', formName, config)
  },
  getMyShopGoodsList (shopId, config) {
    console.log('myfoodshop')
    return Api().get('myfoodshops/' + shopId, config)
  }
}
