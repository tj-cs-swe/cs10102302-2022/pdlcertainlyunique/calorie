import Api from '@/services/Api'

export default {
  getShopServiceList (shopId, config) {
    return Api().get('workoutshops/' + shopId, config)
  },
  MerShopServiceList (shopId, config) {
    return Api().get('myworkoutshops/' + shopId, config)
  },
  Order (formName, config) {
    return Api().post('myServiceOrders/newServiceOrder', formName, config)
  },
  getRecommand (config) {
    return Api().get('serviceorder/recommend', config)
  }
}
