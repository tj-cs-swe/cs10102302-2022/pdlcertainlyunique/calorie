import Api from '@/services/Api'

export default {
  login (formName) {
    return Api().post('login', formName)
  }
}
