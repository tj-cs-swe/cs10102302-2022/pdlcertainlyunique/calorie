import Api from '@/services/Api'

export default {
  getcheck (shopId, serviceId, config) {
    let path = 'myworkoutshops/'
    path += shopId
    path += '/'
    path += serviceId
    path += '/updateService'
    return Api().get(path, config)
  },
  getserviceinfo (shopId, serviceId, config) {
    return Api().get('myworkoutshops' + '/' + shopId + '/' + serviceId, config)
  },
  postservicechange (formName, config, shopId, serviceId) {
    let path = 'myworkoutshops/'
    path += shopId
    path += '/'
    path += serviceId
    path += '/updateService'
    return Api().post(path, formName, config)
  }
}
