import Api from '@/services/Api'

export default {
  getOrders (userId, config) {
    console.log('getOrders')
    return Api().post('usersFoodOrders/' + userId, {
      foodOrderStatus: -1
    }, config)
  },
  Confirm (formData, oId, nS, config) {
    return Api().post('foodorder/' + oId + '/confirmation/' + nS, formData, config)
  },
  UserComment (formData, oId, config) {
    return Api().post('foodorder/' + oId + '/makeComments', formData, config)
  },
  MerchantSendWay (formData, oId, dWay, config) {
    return Api().post('foodorder/' + oId + '/deliveryMethod/' + dWay, formData, config)
  },
  MerchantRider (formData, oId, rId, config) {
    console.log(oId + '|' + rId)
    return Api().post('foodorder/' + oId + '/assignToRider/' + rId, formData, config)
  },
  getMerOrders (shopId, config) {
    return Api().post('myFoodOrders/' + shopId, {
      foodOrderStatus: -1
    }, config)
  },
  getRecommand (config) {
    return Api().get('foodorder/recommend', config)
  },
  getRiderOrders (userId, config) {
    return Api().get('ridersFoodOrders/' + userId, config)
  },
  RiderConfirm (oId, nS, config) {
    console.log(oId + ',' + nS)
    return Api().post('foodorder/' + oId + '/getOrder/' + nS, {}, config)
  }
}
