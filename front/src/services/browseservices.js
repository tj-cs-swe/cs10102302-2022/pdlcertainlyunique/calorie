import Api from '@/services/Api'

export default {
  getservicelist (id, body, config) {
    // console.log('/myServiceOrders/' + id)
    // console.log(id)
    return Api().post('/myServiceOrders/' + id, body, config)
  },
  MerOrder (body, config) {
    return Api().post('/myServiceOrders/confirmation', body, config)
  }
}
