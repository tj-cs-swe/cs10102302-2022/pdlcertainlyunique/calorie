import Api from '@/services/Api'

export default {
  showcomments (shopId, config) {
    return Api().get('foodshops/' + shopId + '/comments', config)
  }
}
