import Api from '@/services/Api'

export default {
  forget (formName) {
    return Api().post('password/new', formName)
  }
}
