import Api from '@/services/Api'

export default {
  postName (formName, config) {
    return Api().post('account/editName', formName, config)
  },
  postPassword (formName, config) {
    return Api().post('account/password', formName, config)
  }
}
