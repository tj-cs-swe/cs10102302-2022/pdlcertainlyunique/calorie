import Api from '@/services/Api'

export default {
  postrider (formName, config) {
    return Api().post('rider', formName, config)
  }
}
