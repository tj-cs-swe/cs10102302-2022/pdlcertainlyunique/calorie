import Api from '@/services/Api'

export default {
  getcheck (shopId, foodId, config) {
    let path = 'myfoodshops/'
    path += shopId
    path += '/'
    path += foodId
    path += '/updateFood'
    return Api().get(path, config)
  },
  getfoodinfo (shopId, foodId, config) {
    return Api().get('myfoodshops' + '/' + shopId + '/' + foodId, config)
  },
  postfoodchange (formName, config, shopId, foodId) {
    let path = 'myfoodshops/'
    path += shopId
    path += '/'
    path += foodId
    path += '/updateFood'
    return Api().post(path, formName, config)
  },
  postchangestatus (formName, config, shopId) {
    let path = 'myfoodshops/'
    path += shopId
    return Api().post(path, formName, config)
  }
}
